#ifndef JORGE_H
#define JORGE_H

#include "jorge/audio.h"
#include "jorge/engine.h"
#include "jorge/exception.h"
#include "jorge/file.h"
#include "jorge/game.h"
#include "jorge/graphics.h"
#include "jorge/input.h"
#include "jorge/log.h"
#include "jorge/math.h"
#include "jorge/resourcesmanager.h"
#include "jorge/screen.h"
#include "jorge/texture.h"

#endif
