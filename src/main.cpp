#include "config.h" // Built by CMake

#include <sstream>
#include <string>

#include <jorge.h>
#include <examplegame/examplegame.h>

/**
 * Root of everything!
 */
int main(int argc, char* argv[]) {

	int debug = false;

	if (argc > 1) { // Check if there are parameters
		for (int i = 1; i < argc; i++) {
			if (std::string(argv[i]) == "-d") {
				debug = true;
			}
			else if (std::string(argv[i]) == "--debug") {
				debug = true;
			}
			else {
				jorge::Log::w("Invalid argument " + std::string(argv[i]));
			}
		}
	}

	jorge::Log::debug = debug;

	// Build version string
	int versionMinor = GAME_VERSION_MINOR;
	int versionMajor = GAME_VERSION_MAJOR;

	std::stringstream version;
	version << versionMajor << "." << versionMinor;

	// Create game and start the engine
	ExampleGame* game = new ExampleGame(version.str());

	jorge::Engine engine = jorge::Engine(game, 800, 600, "JorgeFramework");

	// Exit everything
	delete game; // Not from disk hopefully
	return 0;
}
