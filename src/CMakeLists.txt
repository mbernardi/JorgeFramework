cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(ExampleGame)

# Set version numbers
set (ExampleGame_VERSION_MAJOR 0)
set (ExampleGame_VERSION_MINOR 9)

# Bump up warning levels appropriately for clang, gcc & msvc and build
# in debug mode
if (
	${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU" OR
	${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang"
	)

	set(CMAKE_CXX_FLAGS
	    "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -std=c++11 -fdiagnostics-color=always")
	set(CMAKE_CXX_FLAGS_DEBUG
	    "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG} -g")
	set(CMAKE_CXX_FLAGS_RELEASE
	    "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE} -O2")

elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL "MSVC")
	if (CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
		string(REGEX REPLACE "/W[0-4]" "/W4"
		       CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
	else()
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
	endif()
endif()

# Store version numbers in header
configure_file ("${PROJECT_SOURCE_DIR}/config.h.in" # Header to modify
                "${PROJECT_BINARY_DIR}/config.h" # Modified header
                )

# Set directories for binaries
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/../bin)

# Load cmake modules (FindSDL2.cmake, FindSDL2_image.cmake, ...)
set(CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/../cmake")

# Run FindSDL2
find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
find_package(SDL2_ttf REQUIRED)
find_package(SDL2_mixer REQUIRED)

# Include headers
include_directories(${PROJECT_SOURCE_DIR}) # Current dir
include_directories(${PROJECT_BINARY_DIR}) # PruebaConfig.h header location

# Include SDL2 headers
include_directories(${SDL2_INCLUDE_DIR})
include_directories(${SDL2_IMAGE_INCLUDE_DIR})
include_directories(${SDL2_TTF_INCLUDE_DIR})
include_directories(${SDL2_MIXER_INCLUDE_DIR})

# Search for more CMakeLists in these subdirectories
add_subdirectory(jorge)
add_subdirectory(examplegame)

# Include cpp
add_executable(main main.cpp)

# Link headers
target_link_libraries(main
                      examplegame
                      jorge
                      ${SDL2_LIBRARY}
                      ${SDL2_IMAGE_LIBRARY}
                      ${SDL2_TTF_LIBRARY}
                      ${SDL2_MIXER_LIBRARY})
