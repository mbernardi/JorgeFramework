#include "exception.h"

#include <exception>
#include <stdexcept>
#include <string>

namespace jorge {

JorgeError::JorgeError(std::string message) : runtime_error(message) { }

SdlError::SdlError(std::string message) : runtime_error(message) { }

GameError::GameError(std::string message) : runtime_error(message) { }

JorgeException::JorgeException(std::string message)
		: runtime_error(message) { }

JorgeException::JorgeException(std::string function, std::string message)
		: runtime_error(function + " : " + message) { }

FileWriteException::FileWriteException(std::string function,
		std::string message, int success)
		: JorgeException(function, message), success(success) { }

int FileWriteException::getSuccess() {
	return success;
}

FileReadException::FileReadException(std::string function,
		std::string message, int success)
		: JorgeException(function, message), success(success) { }

int FileReadException::getSuccess() {
	return success;
}

} // namespace jorge
