#ifndef JORGE_RESOURCES_MANAGER_H
#define JORGE_RESOURCES_MANAGER_H

#include <string>
#include <unordered_map>

#include "texture.h"
#include "audio.h"
#include "engine.h"

namespace jorge {

/**
 * Saves and manages resources
 */
class ResourcesManager {
	public:
		/**
		 * Loads and adds textures
		 * @param engine
		 * @param paths Unordered map of paths and their ids
		 */
		void loadTextures(Engine* engine,
				std::unordered_map<std::string, Path> paths);

		/**
		 * Adds an existent texture
		 */
		void addTexture(std::string id, Texture* texture);

		/**
		 * Returns a saved texture
		 */
		Texture* getTexture(std::string id);

		/**
		 * Removes a texture
		 */
		void removeTexture(std::string id);

		/**
		 * Adds an existent sound
		 */
		void addSound(std::string id, Sound* sound);

		/**
		 * Returns a saved sound
		 */
		Sound* getSound(std::string id);

		/**
		 * Removes a sound
		 */
		void removeSound(std::string id);

		/**
		 * Adds an existent music
		 */
		void addMusic(std::string id, Music* music);

		/**
		 * Returns a saved music
		 */
		Music* getMusic(std::string id);

		/**
		 * Removes a music
		 */
		void removeMusic(std::string id);

		~ResourcesManager();

	private:
		std::unordered_map<std::string, Texture*> textures;
		std::unordered_map<std::string, Sound*> sounds;
		std::unordered_map<std::string, Music*> songs;
};

} // namespace jorge

#endif
