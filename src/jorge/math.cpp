#include "math.h"

#include <string>
#include <sstream>
#include <math.h>

#include <SDL.h>

#include "exception.h"

namespace jorge {

Vector2::Vector2() : x(0), y(0) {}

Vector2::Vector2(float x, float y) : x(x), y(y) {}

void Vector2::set(float x, float y) {
	this->x = x;
	this->y = y;
}

float Vector2::length() {
	return sqrt(x*x + y*y);
}

std::string Vector2::toString() {
	std::stringstream sstream;
	sstream << "(" << x << ", " << y << ")";
	return sstream.str();
}

Rectangle::Rectangle()
		: x(0), y(0), w(0), h(0), savedAlign(Rectangle::ALIGN_TOP_LEFT) {}

Rectangle::Rectangle(float x, float y, float w, float h, alignOptions align) {
	set(x, y, w, h, align);
}

void Rectangle::set(Vector2 position) {
	set(position.x, position.y, w, h);
}

void Rectangle::set(alignOptions align) {
	savedAlign = align;
}

void Rectangle::set(float x, float y, float w, float h) {
	set(x, y, w, h, savedAlign);
}

void Rectangle::set(float x, float y, float w, float h, alignOptions align) {
	this->w = w;
	this->h = h;
	this->savedAlign = align;
	switch (align) {
		case ALIGN_TOP_LEFT:
			this->x = x;
			this->y = y;
			break;
		case ALIGN_TOP_RIGHT:
			this->x = x - w;
			this->y = y;
			break;
		case ALIGN_BOTTOM_LEFT:
			this->x = x;
			this->y = y - h;
			break;
		case ALIGN_BOTTOM_RIGHT:
			this->x = x - w;
			this->y = y - h;
			break;
		case ALIGN_CENTER:
			this->x = x - w/2;
			this->y = y - h/2;
			break;
	}
}

float Rectangle::getX() {
	return getX(savedAlign);
}

float Rectangle::getX(alignOptions align) {
	switch (align) {
		case ALIGN_TOP_LEFT:
		case ALIGN_BOTTOM_LEFT:
			return x;
		case ALIGN_TOP_RIGHT:
		case ALIGN_BOTTOM_RIGHT:
			return x + w;
		case ALIGN_CENTER:
			return x + w/2;
	}
	throw JorgeException("Rectangle::GetX", "Invalid aling option?");
}

float Rectangle::getY() {
	return getY(savedAlign);
}

float Rectangle::getY(alignOptions align) {
	switch (align) {
		case ALIGN_TOP_LEFT:
		case ALIGN_TOP_RIGHT:
			return y;
		case ALIGN_BOTTOM_LEFT:
		case ALIGN_BOTTOM_RIGHT:
			return y + h;
		case ALIGN_CENTER:
			return y + h/2;
	}
	throw JorgeException("Rectangle::GetY", "Invalid aling option?");
}

float Rectangle::getW() {
	return w;
}

float Rectangle::getH() {
	return h;
}

Vector2 Rectangle::getCorner(alignOptions align) {
	return Vector2(getX(align), getY(align));
}

bool Rectangle::contains(Vector2 point) {
	if (
			point.x > x &&
			point.x < x + w &&
			point.y > y &&
			point.y < y + h)
	{
		return true;
	}
	else {
		return false;
	}
}

std::string Rectangle::toString() {
	std::stringstream sstream;
	sstream << "(" << x << ", " << y << ", " << w << ", " << h << ")";
	return sstream.str();
}

SDL_Rect Rectangle::getSdlRect() {
	return {
		(int) round(x),
		(int) round(y),
		(int) round(w),
		(int) round(h)
	};
}

Circle::Circle() : x(0), y(0), r(0) {}

Circle::Circle(float x, float y, float r) : x(x), y(y), r(r) {}

void Circle::set(Vector2 position) {
	set(position.x, position.y);
}

void Circle::set(float x, float y) {
	set(x, y, r);
}

void Circle::set(float x, float y, float r) {
	this->x = x;
	this->y = y;
	this->r = r;
}

float Circle::getX() {
	return x;
}

float Circle::getY() {
	return y;
}

float Circle::getRadius() {
	return r;
}

Vector2 Circle::getCenter() {
	return Vector2(x, y);
}

bool Circle::contains(Vector2 point) {
	if (Math::distance(getCenter(), point) < r) {
		return true;
	}
	else {
		return false;
	}
}

std::string Circle::toString() {
	std::stringstream sstream;
	sstream << "(" << x << ", " << y << ", " << r << ")";
	return sstream.str();
}

Vector2 Math::add(Vector2 vect1, Vector2 vect2) {
	return Vector2(vect1.x + vect2.x, vect1.y + vect2.y);
}

Vector2 Math::substract(Vector2 vect1, Vector2 vect2) {
	return Vector2(vect1.x - vect2.x, vect1.y - vect2.y);
}

float Math::distance(Vector2 vect1, Vector2 vect2) {
	return substract(vect1, vect2).length();
}

bool Math::intersects(Rectangle rect1, Rectangle rect2) {
	if (
			rect1.x < rect2.x + rect2.w &&
			rect1.y < rect2.y + rect2.h &&
			rect2.x < rect1.x + rect1.w &&
			rect2.y < rect1.y + rect1.h)
	{
		return true;
	}
	else {
		return false;
	}
}

bool Math::intersects(Circle circle1, Circle circle2) {
	if (
			distance(circle1.getCenter(), circle2.getCenter()) <=
			circle1.getRadius() + circle2.getRadius())
	{
		return true;
	}
	else {
		return false;
	}
}

bool Math::intersects(Rectangle rect, Circle circle) {
	return intersects(circle, rect); // Reorder arguments
}

bool Math::intersects(Circle circle, Rectangle rect) {
	float closestX = circle.getX();
	float closestY = circle.getY();

	if (circle.x < rect.x) {
		closestX = rect.x;
	}
	else if (circle.x > rect.x + rect.w) {
		closestX = rect.x + rect.w;
	}

	if (circle.y < rect.y) {
		closestY = rect.y;
	}
	else if (circle.y > rect.y + rect.h) {
		closestY = rect.y + rect.h;
	}

	if (circle.contains(Vector2(closestX, closestY))) {
		return true;
	}
	else {
		return false;
	}
}

} // namespace jorge
