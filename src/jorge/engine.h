#ifndef JORGE_ENGINE_H
#define JORGE_ENGINE_H

#include <cinttypes>

#include <SDL.h>

#include "game.h"
#include "graphics.h"
#include "input.h"

namespace jorge {

class Graphics; // Because of circular dependency
class Game; // Because of circular dependency
class Input; // Because of circular dependency

/**
 * Handles main loop and important SDL things
 * Keeps track ot the SDL window and renderer
 * Doesn't support window size changes
 */
class Engine {
	public:
		/**
		 * Creates engine
		 * Loads SDL, window and renderer
		 * @param game Game instance who has loop()
		 * @param screenWidth
		 * @param screenHeight
		 * @param title Window title
		 * @param maxDt Maximum deltatime, defaults to 0.033... (1/30 fps);
		 */
		Engine(Game* game, int screenWidth, int screenHeight, std::string title,
				double maxDt = 1./30.);

		/**
		 * Starts main loop
		 */
		void start();

		/**
		 * Stops the main loop, called by the game
		 */
		void stop();

		int getScreenWidth();

		int getScreenHeight();

		/**
		 * Get input class, neccessary when setting InputListener
		 */
		Input* getInput();

		/**
		 * You should'nt call this
		 */
		SDL_Window* getSdlWindow();

		/**
		 * You should'nt call this
		 * Used by Texture
		 */
		SDL_Renderer* getSdlRenderer();

		/**
		 * You should'nt call this
		 * Gets rid of SDL things (window, renderer, texture or surface)
		 * Handles lots of arguments at once
		 * Is safe to pass nullptr
		 *
		 * http://www.willusher.io/pages/sdl2/
		 * https://github.com/Twinklebear/TwinklebearDev-Lessons
		 *
		 * @param {...*} thing
		 */
		template<typename T, typename... Args>
		void sdlCleanup(T *t, Args&&... args);

		~Engine();

	private:
		bool quit; // Set true by stop()

		SDL_Window* window = nullptr;
		SDL_Renderer* renderer = nullptr;

		Input* input = nullptr;
		Game* game = nullptr;
		int screenWidth;
		int screenHeight;
		std::string title;

		// Time and FPS things
		uint32_t time;
		uint32_t lastTime;
		float fps;
		double dt;
		double maxDt;
};


/*
 * These specializations serve to free the passed argument and also
 * provide the base cases for the recursive call above, eg. when args is
 * only a single item one of the specializations below will be called
 * bycleanup(std::forward<Args>(args)...), ending the recursion
 * We also make it safe to pass nullptrs to handle situations where
 * wedon't want to bother finding out which values failed to load
 *
 * http://www.willusher.io/pages/sdl2/
 * https://github.com/Twinklebear/TwinklebearDev-Lessons
*/
template<>
void Engine::sdlCleanup<SDL_Window>(SDL_Window *win);

template<>
void Engine::sdlCleanup<SDL_Renderer>(SDL_Renderer *ren);

template<>
void Engine::sdlCleanup<SDL_Texture>(SDL_Texture *tex);

template<>
void Engine::sdlCleanup<SDL_Surface>(SDL_Surface *surf);

} // namespace jorge

#endif
