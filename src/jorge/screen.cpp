#include "screen.h"

#include "game.h"

namespace jorge {

Screen::Screen() {
	stopped = true;
	hidden = true;
}

void Screen::start() {
	stopped = false;
}

void Screen::show() {
	hidden = false;
}

void Screen::hide() {
	hidden = true;
}

void Screen::stop() {
	stopped = true;
}

bool Screen::isHidden() {
	return hidden;
}

bool Screen::isStopped() {
	return stopped;
}

Screen::~Screen() { }

} // namespace jorge

