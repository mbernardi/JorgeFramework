#ifndef JORGE_TEXTURE_H
#define JORGE_TEXTURE_H

#include <string>

#include <SDL.h>
#include <SDL_image.h>

#include "engine.h"
#include "math.h"
#include "file.h"

namespace jorge {

/**
 * Manages an SDL_Texture
 */
class Texture {
	public:
		/**
		 * Create texture from image
		 * @param engine
		 * @param imagePath Path to image file
		 */
		Texture(Engine* engine, Path imagePath);

		/**
		 * Draws the texture in the specified point
		 * Specifying align option is optional
		 * @param engine
		 * @param pos Position where to draw the texture
		 * @param align How to align the texture
		 */
		void draw(Engine* engine, Vector2 pos,
				Rectangle::alignOptions align = Rectangle::ALIGN_TOP_LEFT);

		/**
		 * Draws texture with the specified rectangle shape
		 * Measured in pixels
		 * @param engine
		 * @param rect Where the texture will be drawn
		 */
		void draw(Engine* engine, Rectangle rect);

		/**
		 * Draws texture region with the specified rectangle shape
		 * Measured in pixels
		 * @param engine
		 * @param rect Where the texture will be drawn
		 * @param section (Optional) Part of the texture to fraw, defaults to null,
		 *        drawing everything
		 */
		void draw(Engine* engine, Rectangle rect, Rectangle section);

		/**
		 * Returns Vector2 specifying original image size in pixels
		 */
		Vector2 getOriginalSize();

		/**
		 * You should'nt call this
		 */
		SDL_Texture* getSdlTexture();

		~Texture();

	protected:
		/**
		 * Because TextTexture needs it
		 */
		Texture();

		SDL_Texture* sdlTexture = nullptr;
		Engine* engine = nullptr;
	};

class TextTexture : public Texture {
	public:
		/**
		 * Create texture from text, doesn't supports newlines, use method with
		 * maxWidth argument (and set it to large but not too large number)
		 * @param engine
		 * @param text
		 * @param fontPath Path to font file
		 * @param color
		 * @param size
		 */
		TextTexture(Engine* engine, std::string text, Path fontPath,
				Color color, int size);

		/**
		 * Create texture from text, wrapping at maxWidth, accepts newlines (\n)
		 * @param engine
		 * @param text
		 * @param fontPath Path to font file
		 * @param color
		 * @param size
		 * @param maxWidth
		 */
		TextTexture(Engine* engine, std::string text, Path fontPath,
				Color color, int size, int maxWidth);

		~TextTexture();
};

} // namespace jorge

#endif
