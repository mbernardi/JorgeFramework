#ifndef JORGE_MATH_H
#define JORGE_MATH_H

/**
 * Math related things
 */

#include <string>

#include <SDL.h>

namespace jorge {

class Vector2 {
	public:
		float x, y;

		/**
		 * Initializes everything in zero
		 */
		Vector2();

		/**
		 * Creates a point
		 * @param x
		 * @param y
		 */
		Vector2(float x, float y);

		/**
		 * Sets parameters
		 * @param x
		 * @param y
		 * @param w Width
		 * @param h Height
		 */
		void set(float x, float y);

		/**
		 * Returns it's length
		 */
		float length();

		/**
		 * Output as string, useful for logging
		 * Format: (x, y)
		 */
		std::string toString();
};

class Rectangle {
	public:
		// x and y always point to top left corner regardless of align
		float x, y, w, h;
		enum alignOptions {ALIGN_TOP_LEFT,
						   ALIGN_TOP_RIGHT,
						   ALIGN_BOTTOM_LEFT,
						   ALIGN_BOTTOM_RIGHT,
						   ALIGN_CENTER};

		/**
		 * Initializes everything in zero
		 */
		Rectangle();

		/**
		 * Creates a rectangle
		 * @param x
		 * @param y
		 * @param w Width
		 * @param h Height
		 * @param align Where to align the rectangle
		 */
		Rectangle(float x, float y, float w, float h,
				alignOptions align = Rectangle::ALIGN_TOP_LEFT);

		/**
		 * Sets position
		 * @param position
		 */
		void set(Vector2 position);

		/**
		 * Sets align mode (doesn't move the rectangle)
		 * @param setAlign
		 */
		void set(alignOptions align);

		/**
		 * Sets parameters
		 * @param x
		 * @param y
		 * @param w Width
		 * @param h Height
		 */
		void set(float x, float y, float h, float w);

		/**
		 * Sets parameters
		 * @param x
		 * @param y
		 * @param w Width
		 * @param h Height
		 * @param align Where to align specified x and y parameters, defaults to
		 * saved align
		 */
		void set(float x, float y, float h, float w, alignOptions align);

		/**
		 * Get x of point related to saved align
		 */
		float getX();

		/**
		 * Get x of point related to specified align
		 * @param align
		 */
		float getX(alignOptions align);

		/**
		 * Get y of point related to saved align
		 */
		float getY();

		/**
		 * Get y of point related to specified align
		 * @param align
		 */
		float getY(alignOptions align);

		/**
		 * Get width
		 */
		float getW();

		/**
		 * Get height
		 */
		float getH();

		/**
		 * Get vector to specified corner
		 * @param align
		 */
		Vector2 getCorner(alignOptions align);

		/**
		 * Check if contains a point
		 */
		bool contains(Vector2 point);

		/**
		 * Output as string, useful for logging
		 * x and y always represent top left corner
		 * Format: (x, y, w, h)
		 */
		std::string toString();

		/**
		 * You should'nt call this
		 * Used by graphics
		 */
		SDL_Rect getSdlRect();

	private:
		alignOptions savedAlign;
};

class Circle {
	public:
		float x, y, r;

		/**
		 * Initializes everything in zero
		 */
		Circle();

		/**
		 * Creates a circle
		 * @param x
		 * @param y
		 * @param r Radius
		 */
		Circle(float x, float y, float r);

		/**
		 * Sets position
		 * @param position
		 */
		void set(Vector2 position);

		/**
		 * Sets parameters
		 * @param x
		 * @param y
		 */
		void set(float x, float y);

		/**
		 * Sets parameters
		 * @param x
		 * @param y
		 * @param r Radius
		 */
		void set(float x, float y, float r);

		/**
		 * Get x of center
		 */
		float getX();

		/**
		 * Get y of center
		 */
		float getY();

		/**
		 * Get radius
		 */
		float getRadius();

		/**
		 * Get vector to center
		 */
		Vector2 getCenter();

		/**
		 * Check if contains a point
		 */
		bool contains(Vector2 point);

		/**
		 * Output as string, useful for logging
		 * Format: (x, y, r)
		 */
		std::string toString();
};

class Math {
	public:
		/**
		 * Add two vectors
		 */
		static Vector2 add(Vector2 vect1, Vector2 vect2);

		/**
		 * Substract two vectors
		 */
		static Vector2 substract(Vector2 vect1, Vector2 vect2);

		/**
		 * Measure distance between two vectors
		 */
		static float distance(Vector2 vect1, Vector2 vect2);

		/**
		 * Check if two shapes intersect
		 */
		static bool intersects(Rectangle rect1, Rectangle rect2);

		/**
		 * Check if two shapes intersect
		 */
		static bool intersects(Circle circle1, Circle circle2);

		/**
		 * Check if two shapes intersect
		 */
		static bool intersects(Rectangle rect, Circle circle); // Reorders arguments

		/**
		 * Check if two shapes intersect
		 */
		static bool intersects(Circle circle, Rectangle rect);
};

} // namespace jorge

#endif
