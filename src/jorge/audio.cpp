#include "audio.h"

#include <string>

#include <SDL.h>
#include <SDL_mixer.h>

#include "exception.h"

namespace jorge {

Sound::Sound(Path soundPath) {
	sdlSound = Mix_LoadWAV(soundPath.get());
	if (sdlSound == nullptr) {
		std::string sdlError = SDL_GetError();
		std::string mixError = Mix_GetError();
		throw SdlError("Cannot load sound: " + soundPath.getString() + ", " +
				sdlError + ", " + mixError);
	}
}

void Sound::play() {
	play(1); // Play once
}

void Sound::play(int repeat) {
	if (repeat < 1) {
		throw InvalidArgumentException("Sound::play()",
				"Repeat number must be greater than 0");
	}
	Mix_PlayChannel(-1, sdlSound, repeat - 1); // -1 is for playing the sound in
											   // the first free channel
}

Sound::~Sound() {
	if (sdlSound != nullptr) {
		Mix_FreeChunk(sdlSound);
	}
}


Music::Music(Path musicPath) {
	sdlMusic = Mix_LoadMUS(musicPath.get());
	if (sdlMusic == nullptr) {
		std::string sdlError = SDL_GetError();
		std::string mixError = Mix_GetError();
		throw SdlError("Cannot load music: " + musicPath.getString() + ", " +
				sdlError + ", " + mixError);
	}
}

void Music::play(Music* music) {
	play(music, 0); // Play looping forever
}

void Music::play(Music* music, int repeat) {
	// Check argument
	if (music == nullptr) {
		throw InvalidArgumentException("Music::play()",
				"Passed nullptr as music");
	}

	if (Music::isStopped()) {
		if (repeat < 0) {
			throw InvalidArgumentException("Music::play()",
					"Repeat number must be 0 (loops forever) or a positive "
					"number");
		}
		else if (repeat == 0) {
			Mix_PlayMusic(music->sdlMusic, -1); // Loops forever
		}
		else {
			Mix_PlayMusic(music->sdlMusic, repeat); // Loops forever
		}
	}
	else {
		throw InvalidMusicOperationException("Music::play()",
				"Music is already playing (only play one song at once)");
	}
}

void Music::pause() {
	if (Music::isPlaying()) {
		Mix_PauseMusic();
	}
	else {
		throw InvalidMusicOperationException("Music::pause()",
				"There is no music playing");
	}
}

void Music::resume() {
	if (Music::isPaused()) {
		Mix_ResumeMusic();
	}
	else {
		throw InvalidMusicOperationException("Music::pause()",
				"There is no paused music");
	}
}

void Music::stop() {
	if (!Music::isStopped()) {
		Mix_HaltMusic();
	}
	else {
		throw InvalidMusicOperationException("Music::stop()",
				"There is no paused or playing music");
	}
}

bool Music::isPlaying() {
	if (Mix_PlayingMusic() && !Mix_PausedMusic()) {
		return true;
	}
	else {
		return false;
	}
}

bool Music::isPaused() {
	if (Mix_PlayingMusic() && Mix_PausedMusic()) {
		return true;
	}
	else {
		return false;
	}
}

bool Music::isStopped() {
	if (!Mix_PlayingMusic()) {
		return true;
	}
	else {
		return false;
	}
}

Music::~Music() {
	if (sdlMusic != nullptr) {
		Mix_FreeMusic(sdlMusic);
	}
}

} // namespace jorge
