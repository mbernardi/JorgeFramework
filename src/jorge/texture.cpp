#include "texture.h"

#include <SDL.h>
#include <SDL_ttf.h>

#include "engine.h"
#include "exception.h"
#include "math.h"
#include "graphics.h"
#include "log.h"
#include "file.h"

namespace jorge {

Texture::Texture(Engine* engine, Path imagePath) : engine(engine) {
	sdlTexture = IMG_LoadTexture(engine->getSdlRenderer(), imagePath.get());
	if (sdlTexture == nullptr) {
		std::string sdlError = SDL_GetError();
		std::string imgError = IMG_GetError();
		throw SdlError("Cannot load texture: " + imagePath.getString() + ", " +
				sdlError + ", " + imgError);
	}
}

void Texture::draw(Engine* engine, Vector2 pos,	Rectangle::alignOptions align) {
	Vector2 size = getOriginalSize();

	draw(engine, Rectangle(pos.x, pos.y, size.x, size.y, align));
}

void Texture::draw(Engine* engine, Rectangle rect) {
	SDL_Rect sdlRect;
	sdlRect.x = rect.x;
	sdlRect.y = rect.y;
	sdlRect.w = rect.w;
	sdlRect.h = rect.h;

	SDL_RenderCopy(engine->getSdlRenderer(), sdlTexture, NULL, &sdlRect);
}

void Texture::draw(Engine* engine, Rectangle rect, Rectangle section) {
	SDL_Rect sdlRect;
	sdlRect.x = rect.x;
	sdlRect.y = rect.y;
	sdlRect.w = rect.w;
	sdlRect.h = rect.h;

	SDL_Rect sdlSection;
	sdlSection.x = section.x;
	sdlSection.y = section.y;
	sdlSection.w = section.w;
	sdlSection.h = section.h;

	SDL_RenderCopy(engine->getSdlRenderer(), sdlTexture,
			&sdlSection, &sdlRect);
}

Vector2 Texture::getOriginalSize() {
	int w, h;
	SDL_QueryTexture(sdlTexture, NULL, NULL, &w, &h);
	return Vector2(w, h);
}

SDL_Texture* Texture::getSdlTexture() {
	return sdlTexture;
}

Texture::Texture() { }

Texture::~Texture() {
	engine->sdlCleanup(sdlTexture);
}

TextTexture::TextTexture(Engine* engine, std::string text, Path fontPath,
		Color color, int size) {

	TTF_Font* sdlFont = TTF_OpenFont(fontPath.get(), size);
	if (sdlFont == nullptr) {
		std::string sdlError = SDL_GetError();
		throw SdlError("Cannot open font: " + fontPath.getString() + ", " +
				sdlError);
	}

	SDL_Surface* surface = TTF_RenderText_Blended(sdlFont, text.c_str(),
			color.getSdlColor());
	if (surface == nullptr) {
		TTF_CloseFont(sdlFont);
		std::string sdlError = SDL_GetError();
		throw SdlError("Cannot create surface for text: " +
				text + ", " + sdlError);
	}

	sdlTexture = SDL_CreateTextureFromSurface(engine->getSdlRenderer(),
			surface);
	if (sdlTexture == nullptr) {
		TTF_CloseFont(sdlFont);
		SDL_FreeSurface(surface);
		std::string sdlError = SDL_GetError();
		throw SdlError("Cannot create texture for text: " +
				text + ", " + sdlError);
	}

	TTF_CloseFont(sdlFont);
	SDL_FreeSurface(surface);
}

TextTexture::TextTexture(Engine* engine, std::string text, Path fontPath,
		Color color, int size, int maxWidth) {

	TTF_Font* sdlFont = TTF_OpenFont(fontPath.get(), size);
	if (sdlFont == nullptr) {
		std::string sdlError = SDL_GetError();
		throw SdlError("Cannot open font: " + fontPath.getString() + ", " +
				sdlError);
	}

	SDL_Surface* surface = TTF_RenderText_Blended_Wrapped(sdlFont, text.c_str(),
			color.getSdlColor(), maxWidth);
	if (surface == nullptr) {
		TTF_CloseFont(sdlFont);
		std::string sdlError = SDL_GetError();
		throw SdlError("Cannot create surface for text: " +
				text + ", " + sdlError);
	}

	sdlTexture = SDL_CreateTextureFromSurface(engine->getSdlRenderer(),
			surface);
	if (sdlTexture == nullptr) {
		TTF_CloseFont(sdlFont);
		SDL_FreeSurface(surface);
		std::string sdlError = SDL_GetError();
		throw SdlError("Cannot create texture for text: " +
				text + ", " + sdlError);
	}

	TTF_CloseFont(sdlFont);
	SDL_FreeSurface(surface);
}

TextTexture::~TextTexture() {
	engine->sdlCleanup(sdlTexture);
}

} // namespace jorge
