#include "log.h"

#include <string>
#include <iostream>

#include <SDL.h>

namespace jorge {

bool Log::debug = false;

void Log::e(const std::string& message) {
	std::cout << "ERROR: " << message << std::endl;
}

void Log::w(const std::string& message) {
	std::cout << "WARNING: " << message << std::endl;
}

void Log::i(const std::string& message) {
	std::cout << message << std::endl;
}

void Log::d(const std::string& message) {
	if (debug) {
		std::cout << "DEBUG: " << message << std::endl;
	}
}

} // namespace jorge
