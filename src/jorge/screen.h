#ifndef JORGE_SCREEN_H
#define JORGE_SCREEN_H

#include "game.h"

namespace jorge {

class Game; // Because of circular dependency

class Screen {
	public:
		Screen();

		/**
		 * Called on start, but not when the window was hidden and not stopped
		 */
		virtual void start();

		virtual void loop(double& dt) = 0;

		/**
		 * Called after show()
		 * When window is hidden and shown again, show() is called
		 */
		virtual void show();

		/**
		 * Called when hiding the window, and before stop() if the window stops
		 * permanently
		 */
		virtual void hide();

		/**
		 * Called when the window stops forever
		 */
		virtual void stop();

		bool isHidden();

		bool isStopped();

		/**
		 * Gets rid of resources
		 */
		virtual ~Screen();

	private:
		bool hidden;
		bool stopped;
};

} // namespace jorge

#endif
