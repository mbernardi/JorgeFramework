#include "resourcesmanager.h"

#include <SDL.h>
#include <unordered_map>

#include "texture.h"
#include "audio.h"
#include "engine.h"
#include "exception.h"

namespace jorge {

void ResourcesManager::loadTextures(Engine* engine,
		std::unordered_map<std::string, Path> paths) {

	for (auto& i : paths) {
		addTexture(i.first, new Texture(engine, i.second));
	}
}

void ResourcesManager::addTexture(std::string id, Texture* texture) {
	textures.insert(make_pair(id, texture));
}

Texture* ResourcesManager::getTexture(std::string id) {
	try {
		return textures.at(id);
	}
	catch (std::out_of_range e) {
		throw JorgeException("ResourcesManager::getTexture",
				"Cant get texture with id: " + id);
	}
}

void ResourcesManager::removeTexture(std::string id) {
	textures.erase(id);
}

void ResourcesManager::addSound(std::string id, Sound* sound) {
	sounds.insert(make_pair(id, sound));
}

Sound* ResourcesManager::getSound(std::string id) {
	try {
		return sounds.at(id);
	}
	catch (std::out_of_range e) {
		throw JorgeException("ResourcesManager::getSound",
				"Cant get sound with id: " + id);
	}
}

void ResourcesManager::removeSound(std::string id) {
	sounds.erase(id);
}

void ResourcesManager::addMusic(std::string id, Music* music) {
	songs.insert(make_pair(id, music));
}

Music* ResourcesManager::getMusic(std::string id) {
	try {
		return songs.at(id);
	}
	catch (std::out_of_range e) {
		throw JorgeException("ResourcesManager::getMusic",
				"Cant get music with id: " + id);
	}
}

void ResourcesManager::removeMusic(std::string id) {
	songs.erase(id);
}

ResourcesManager::~ResourcesManager() {
	textures.clear(); // Deletes all textures in the map
	sounds.clear(); // Deletes all sounds in the map
	songs.clear(); // Deletes all music in the map
}

} // namespace jorge
