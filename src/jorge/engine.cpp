#include "engine.h"

#include <string>

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

#include "log.h"
#include "exception.h"
#include "game.h"
#include "graphics.h"

namespace jorge {

// Initialize static variables, why I do need to do this C++??? Why???
uint32_t time;
uint32_t lastTime = 0; // Because otherwise we will read it before giving it a
					   // value
float fps;
double dt;
double maxDt;

Engine::Engine(Game* game, int screenWidth, int screenHeight,
		std::string title, double maxDt)
		: game(game), screenWidth(screenWidth), screenHeight(screenHeight),
		title(title), maxDt(maxDt) {

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		std::string sdlError = SDL_GetError();
		throw SdlError("SDL_Init, " + sdlError);
	}

	if (TTF_Init() != 0){
		std::string sdlError = SDL_GetError();
		SDL_Quit();
		throw SdlError("TTF_Init, " + sdlError);
	}

	/**
	 * Init SDL_Mixer
	 * 44100: Sound Frequency
	 * MIX_DEFAULT_FORMAT: Sample format
	 * 2: Number of channels (2 for stereo)
	 * 2048: Size of sound chunks
	 */
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) != 0) {
		std::string sdlError = Mix_GetError();
		SDL_Quit();
		throw SdlError("Mixer init, " + sdlError);
	}

	window = SDL_CreateWindow(title.c_str(), 100, 100,
							  screenWidth, screenHeight,
							  SDL_WINDOW_SHOWN);

	if (window == nullptr) {
		std::string sdlError = SDL_GetError();
		SDL_Quit();
		throw SdlError("SDL_CreateWindow, " + sdlError);
	}

	/*
	 * Create a renderer, -1 specifies that we want to load whichever
	 * video driver supports the flags we're passing
	 *
	 * Flags:
	 *
	 * SDL_RENDERER_ACCELERATED: We want to use hardware
	 * accelerated rendering
	 *
	 * SDL_RENDERER_PRESENTVSYNC: We want the renderer's present
	 * function (update screen) to be synchornized with the monitor's
	 * refresh rate, so fps will be about 60
	*/
	renderer = SDL_CreateRenderer(window, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr){
		//Log::sdlError("SDL_CreateRenderer");
		sdlCleanup(window);
		std::string sdlError = SDL_GetError();
		SDL_Quit();
		throw SdlError("SDL_CreateRenderer, " + sdlError);
	}

	input = new Input(this);
	// Finished setting up, start!
	start();
}


void Engine::start() {
	game->start(this);

	// Main Loop!!!!!!!!!!!!!!!!!!!!!!!!!!
	quit = false;
	while (!quit) {

		/**
		 * Notify events to InputListener and stop the engine on quit
		 */
		input->checkSdlEvents();

		/*
		 * FPS and time things...
		 */
		time = SDL_GetPerformanceCounter();
		// Looks like PerformanceFrecuency returns time unit
		dt = (time - lastTime) / (float) SDL_GetPerformanceFrequency();
		if (dt != 0) {
			fps = 1 / dt;
			//Log::l(std::to_string(fps));
		}
		lastTime = time;

		// Limit deltatime, otherwise things will teleport too far when in lag
		if (dt > maxDt) {
			dt = maxDt;
			Log::d("Limited deltatime this frame (means FPS too low)");
		}

		/*
		 * Render and update everything
		 */
		Graphics::drawStart(renderer);

		game->loop(dt);

		Graphics::drawFinish(renderer);
	}

	game->stop();
}

void Engine::stop() {
	quit = true;
}

int Engine::getScreenWidth() {
	return screenWidth;
}

int Engine::getScreenHeight() {
	return screenHeight;
}

Input* Engine::getInput() {
	return input;
}

SDL_Window* Engine::getSdlWindow() {
	return window;
}

SDL_Renderer* Engine::getSdlRenderer() {
	return renderer;
}

template<typename T, typename... Args>
void Engine::sdlCleanup(T *t, Args&&... args){
	//Cleanup the first item in the list
	sdlCleanup(t);
	//Recurse to clean up the remaining arguments
	sdlCleanup(std::forward<Args>(args)...);
}

template<>
void Engine::sdlCleanup<SDL_Window>(SDL_Window *win) {
	if (!win){
		return;
	}
	SDL_DestroyWindow(win);
}

template<>
void Engine::sdlCleanup<SDL_Renderer>(SDL_Renderer *ren) {
	if (!ren){
		return;
	}
	SDL_DestroyRenderer(ren);
}

template<>
void Engine::sdlCleanup<SDL_Texture>(SDL_Texture *tex) {
	if (!tex){
		return;
	}
	SDL_DestroyTexture(tex);
}

template<>
void Engine::sdlCleanup<SDL_Surface>(SDL_Surface *surf) {
	if (!surf){
		return;
	}
	SDL_FreeSurface(surf);
}

Engine::~Engine() {
	sdlCleanup(window, renderer);
	SDL_Quit();
	delete input;
}

} // namespace jorge
