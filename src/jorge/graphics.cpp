#include "graphics.h"

#include <cstdint>

#include <SDL.h>

#include "engine.h"
#include "log.h"

namespace jorge {

Color::Color() {
	sdlColor = {0, 0, 0, 0};
}

Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	sdlColor = {r, g, b, a};
}

uint8_t Color::getR() {
	return sdlColor.r;
}

uint8_t Color::getG() {
	return sdlColor.g;
}

uint8_t Color::getB() {
	return sdlColor.b;
}

uint8_t Color::getA() {
	return sdlColor.a;
}

SDL_Color Color::getSdlColor() {
	return sdlColor;
}

void Graphics::setIcon(Engine* engine) {
	// I've read that window only supports 32x32 icons
#ifdef _WIN32
	setIcon(engine, Path("icon.bmp"));
#else
	setIcon(engine, Path("icon_windows.bmp"));
#endif
}

void Graphics::setIcon(Engine* engine, Path iconPath) {
	SDL_Surface* iconSurface;
	iconSurface = SDL_LoadBMP(iconPath.get());

	if (iconSurface != NULL) {
		// Looks like color keying doesnt work on SDL_SetWindowIcon
		/*
		SDL_SetColorKey(iconSurface, SDL_TRUE,
				SDL_MapRGB(iconSurface->format, 0, 255, 255));
		*/
		SDL_SetWindowIcon(engine->getSdlWindow(), SDL_LoadBMP(iconPath.get()));
	}
	else {
		std::string sdlError = SDL_GetError();
		Log::e("Error loading icon, " + sdlError);
	}

	SDL_FreeSurface(iconSurface);
}

void Graphics::setDrawColor(Engine* engine, Color color) {
	SDL_SetRenderDrawColor(engine->getSdlRenderer(),
			color.getR(), color.getG(), color.getB(), color.getA());
}

void Graphics::drawRectangle(Engine* engine, Rectangle rect, bool filled) {
	SDL_Rect sdlRect = rect.getSdlRect();
	if (filled) {
		SDL_RenderFillRect(engine->getSdlRenderer(), &sdlRect);
	}
	else {
		SDL_RenderDrawRect(engine->getSdlRenderer(), &sdlRect);
	}
}

void Graphics::drawLine(Engine* engine, Vector2 vect1, Vector2 vect2) {
	SDL_RenderDrawLine(engine->getSdlRenderer(),
			vect1.x, vect1.y, vect2.x, vect2.y);
}

void Graphics::drawPoint(Engine* engine, Vector2 point) {
	SDL_RenderDrawPoint(engine->getSdlRenderer(), point.x, point.y);
}

void Graphics::drawStart(SDL_Renderer* renderer) {
	SDL_SetRenderDrawColor(renderer , 0xFF, 0xFF, 0xFF, 0xFF );
	SDL_RenderClear(renderer);
}

void Graphics::drawFinish(SDL_Renderer* renderer) {
	SDL_RenderPresent(renderer);
}

} // namespace jorge
