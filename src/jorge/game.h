#ifndef JORGE_GAME_H
#define JORGE_GAME_H

#include <string>

#include "engine.h"
#include "screen.h"

namespace jorge{

class Engine; // Because of circular dependency
class Screen; // Because of circular dependency

/**
 * Main game class
 * start(), loop(), stop() and the destructor are called by the engine, this
 * class manages screens, calling start(), loop(), stop() and the destructor
 * in the screen
 */
class Game {
	public:
		/**
		 * Initializes the game, but doesn't starts the screen yet, called
		 * by the engine
		 * @param engine
		 */
		virtual void start(Engine* engine);

		/**
		 * Called by the engine
		 */
		virtual void loop(double& dt);

		/**
		 * Called by the engine
		 */
		virtual void stop();

		/**
		 * Stops the engine
		 */
		void quit();

		/**
		 * Starts new screen and stops the previous one
		 * Throws InvalidArgumentException if a nullptr is given as screen
		 * @param screen Pointer to screen
		 * @param stop Boolean specifying whether to stop the previous screen
		 *        true by default
		 */
		void setScreen(Screen* screen, bool stop = true);

		Screen* getScreen();

		Engine* getEngine();

		virtual ~Game();

	protected:
		Screen* currentScreen = nullptr;
		Engine* engine = nullptr;
};

} // namespace jorge

#endif
