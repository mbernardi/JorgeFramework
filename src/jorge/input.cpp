#include "input.h"

#include <SDL.h>

#include "math.h"
#include "engine.h"

namespace jorge {

Input::Input(Engine* engine) : engine(engine) { }

Vector2 Input::getMousePosition() {
	int x, y;
	SDL_GetMouseState(&x, &y);
	return Vector2(x, y);
}

void Input::setInputListener(InputListener* listener) {
	this->listener = listener;
}

Input::Keycode Input::translateSdlKeycode(SDL_Keycode code) {
	switch(code) {

		// Rename SDL Keyboard Keycodes
		case SDLK_0:                   return Input::KEY_0;
		case SDLK_1:                   return Input::KEY_1;
		case SDLK_2:                   return Input::KEY_2;
		case SDLK_3:                   return Input::KEY_3;
		case SDLK_4:                   return Input::KEY_4;
		case SDLK_5:                   return Input::KEY_5;
		case SDLK_6:                   return Input::KEY_6;
		case SDLK_7:                   return Input::KEY_7;
		case SDLK_8:                   return Input::KEY_8;
		case SDLK_9:                   return Input::KEY_9;
		case SDLK_a:                   return Input::KEY_A;
		case SDLK_AC_BACK:             return Input::KEY_AC_BACK;
		case SDLK_AC_BOOKMARKS:        return Input::KEY_AC_BOOKMARKS;
		case SDLK_AC_FORWARD:          return Input::KEY_AC_FORWARD;
		case SDLK_AC_HOME:             return Input::KEY_AC_HOME;
		case SDLK_AC_REFRESH:          return Input::KEY_AC_REFRESH;
		case SDLK_AC_SEARCH:           return Input::KEY_AC_SEARCH;
		case SDLK_AC_STOP:             return Input::KEY_AC_STOP;
		case SDLK_AGAIN:               return Input::KEY_AGAIN;
		case SDLK_ALTERASE:            return Input::KEY_ALTERASE;
		case SDLK_QUOTE:               return Input::KEY_QUOTE;
		case SDLK_APPLICATION:         return Input::KEY_APPLICATION;
		case SDLK_AUDIOMUTE:           return Input::KEY_AUDIOMUTE;
		case SDLK_AUDIONEXT:           return Input::KEY_AUDIONEXT;
		case SDLK_AUDIOPLAY:           return Input::KEY_AUDIOPLAY;
		case SDLK_AUDIOPREV:           return Input::KEY_AUDIOPREV;
		case SDLK_AUDIOSTOP:           return Input::KEY_AUDIOSTOP;
		case SDLK_b:                   return Input::KEY_B;
		case SDLK_BACKSLASH:           return Input::KEY_BACKSLASH;
		case SDLK_BACKSPACE:           return Input::KEY_BACKSPACE;
		case SDLK_BRIGHTNESSDOWN:      return Input::KEY_BRIGHTNESSDOWN;
		case SDLK_BRIGHTNESSUP:        return Input::KEY_BRIGHTNESSUP;
		case SDLK_c:                   return Input::KEY_C;
		case SDLK_CALCULATOR:          return Input::KEY_CALCULATOR;
		case SDLK_CANCEL:              return Input::KEY_CANCEL;
		case SDLK_CAPSLOCK:            return Input::KEY_CAPSLOCK;
		case SDLK_CLEAR:               return Input::KEY_CLEAR;
		case SDLK_CLEARAGAIN:          return Input::KEY_CLEARAGAIN;
		case SDLK_COMMA:               return Input::KEY_COMMA;
		case SDLK_COMPUTER:            return Input::KEY_COMPUTER;
		case SDLK_COPY:                return Input::KEY_COPY;
		case SDLK_CRSEL:               return Input::KEY_CRSEL;
		case SDLK_CURRENCYSUBUNIT:     return Input::KEY_CURRENCYSUBUNIT;
		case SDLK_CURRENCYUNIT:        return Input::KEY_CURRENCYUNIT;
		case SDLK_CUT:                 return Input::KEY_CUT;
		case SDLK_d:                   return Input::KEY_D;
		case SDLK_DECIMALSEPARATOR:    return Input::KEY_DECIMALSEPARATOR;
		case SDLK_DELETE:              return Input::KEY_DELETE;
		case SDLK_DISPLAYSWITCH:       return Input::KEY_DISPLAYSWITCH;
		case SDLK_DOWN:                return Input::KEY_DOWN;
		case SDLK_e:                   return Input::KEY_E;
		case SDLK_EJECT:               return Input::KEY_EJECT;
		case SDLK_END:                 return Input::KEY_END;
		case SDLK_EQUALS:              return Input::KEY_EQUALS;
		case SDLK_ESCAPE:              return Input::KEY_ESCAPE;
		case SDLK_EXECUTE:             return Input::KEY_EXECUTE;
		case SDLK_EXSEL:               return Input::KEY_EXSEL;
		case SDLK_f:                   return Input::KEY_F;
		case SDLK_F1:                  return Input::KEY_F1;
		case SDLK_F10:                 return Input::KEY_F10;
		case SDLK_F11:                 return Input::KEY_F11;
		case SDLK_F12:                 return Input::KEY_F12;
		case SDLK_F13:                 return Input::KEY_F13;
		case SDLK_F14:                 return Input::KEY_F14;
		case SDLK_F15:                 return Input::KEY_F15;
		case SDLK_F16:                 return Input::KEY_F16;
		case SDLK_F17:                 return Input::KEY_F17;
		case SDLK_F18:                 return Input::KEY_F18;
		case SDLK_F19:                 return Input::KEY_F19;
		case SDLK_F2:                  return Input::KEY_F2;
		case SDLK_F20:                 return Input::KEY_F20;
		case SDLK_F21:                 return Input::KEY_F21;
		case SDLK_F22:                 return Input::KEY_F22;
		case SDLK_F23:                 return Input::KEY_F23;
		case SDLK_F24:                 return Input::KEY_F24;
		case SDLK_F3:                  return Input::KEY_F3;
		case SDLK_F4:                  return Input::KEY_F4;
		case SDLK_F5:                  return Input::KEY_F5;
		case SDLK_F6:                  return Input::KEY_F6;
		case SDLK_F7:                  return Input::KEY_F7;
		case SDLK_F8:                  return Input::KEY_F8;
		case SDLK_F9:                  return Input::KEY_F9;
		case SDLK_FIND:                return Input::KEY_FIND;
		case SDLK_g:                   return Input::KEY_G;
		case SDLK_BACKQUOTE:           return Input::KEY_BACKQUOTE;
		case SDLK_h:                   return Input::KEY_H;
		case SDLK_HELP:                return Input::KEY_HELP;
		case SDLK_HOME:                return Input::KEY_HOME;
		case SDLK_i:                   return Input::KEY_I;
		case SDLK_INSERT:              return Input::KEY_INSERT;
		case SDLK_j:                   return Input::KEY_J;
		case SDLK_k:                   return Input::KEY_K;
		case SDLK_KBDILLUMDOWN:        return Input::KEY_KBDILLUMDOWN;
		case SDLK_KBDILLUMTOGGLE:      return Input::KEY_KBDILLUMTOGGLE;
		case SDLK_KBDILLUMUP:          return Input::KEY_KBDILLUMUP;
		case SDLK_KP_0:                return Input::KEY_KP_0;
		case SDLK_KP_00:               return Input::KEY_KP_00;
		case SDLK_KP_000:              return Input::KEY_KP_000;
		case SDLK_KP_1:                return Input::KEY_KP_1;
		case SDLK_KP_2:                return Input::KEY_KP_2;
		case SDLK_KP_3:                return Input::KEY_KP_3;
		case SDLK_KP_4:                return Input::KEY_KP_4;
		case SDLK_KP_5:                return Input::KEY_KP_5;
		case SDLK_KP_6:                return Input::KEY_KP_6;
		case SDLK_KP_7:                return Input::KEY_KP_7;
		case SDLK_KP_8:                return Input::KEY_KP_8;
		case SDLK_KP_9:                return Input::KEY_KP_9;
		case SDLK_KP_A:                return Input::KEY_KP_A;
		case SDLK_KP_AMPERSAND:        return Input::KEY_KP_AMPERSAND;
		case SDLK_KP_AT:               return Input::KEY_KP_AT;
		case SDLK_KP_B:                return Input::KEY_KP_B;
		case SDLK_KP_BACKSPACE:        return Input::KEY_KP_BACKSPACE;
		case SDLK_KP_BINARY:           return Input::KEY_KP_BINARY;
		case SDLK_KP_C:                return Input::KEY_KP_C;
		case SDLK_KP_CLEAR:            return Input::KEY_KP_CLEAR;
		case SDLK_KP_CLEARENTRY:       return Input::KEY_KP_CLEARENTRY;
		case SDLK_KP_COLON:            return Input::KEY_KP_COLON;
		case SDLK_KP_COMMA:            return Input::KEY_KP_COMMA;
		case SDLK_KP_D:                return Input::KEY_KP_D;
		case SDLK_KP_DBLAMPERSAND:     return Input::KEY_KP_DBLAMPERSAND;
		case SDLK_KP_DBLVERTICALBAR:   return Input::KEY_KP_DBLVERTICALBAR;
		case SDLK_KP_DECIMAL:          return Input::KEY_KP_DECIMAL;
		case SDLK_KP_DIVIDE:           return Input::KEY_KP_DIVIDE;
		case SDLK_KP_E:                return Input::KEY_KP_E;
		case SDLK_KP_ENTER:            return Input::KEY_KP_ENTER;
		case SDLK_KP_EQUALS:           return Input::KEY_KP_EQUALS;
		case SDLK_KP_EQUALSAS400:      return Input::KEY_KP_EQUALSAS400;
		case SDLK_KP_EXCLAM:           return Input::KEY_KP_EXCLAM;
		case SDLK_KP_F:                return Input::KEY_KP_F;
		case SDLK_KP_GREATER:          return Input::KEY_KP_GREATER;
		case SDLK_KP_HASH:             return Input::KEY_KP_HASH;
		case SDLK_KP_HEXADECIMAL:      return Input::KEY_KP_HEXADECIMAL;
		case SDLK_KP_LEFTBRACE:        return Input::KEY_KP_LEFTBRACE;
		case SDLK_KP_LEFTPAREN:        return Input::KEY_KP_LEFTPAREN;
		case SDLK_KP_LESS:             return Input::KEY_KP_LESS;
		case SDLK_KP_MEMADD:           return Input::KEY_KP_MEMADD;
		case SDLK_KP_MEMCLEAR:         return Input::KEY_KP_MEMCLEAR;
		case SDLK_KP_MEMDIVIDE:        return Input::KEY_KP_MEMDIVIDE;
		case SDLK_KP_MEMMULTIPLY:      return Input::KEY_KP_MEMMULTIPLY;
		case SDLK_KP_MEMRECALL:        return Input::KEY_KP_MEMRECALL;
		case SDLK_KP_MEMSTORE:         return Input::KEY_KP_MEMSTORE;
		case SDLK_KP_MEMSUBTRACT:      return Input::KEY_KP_MEMSUBTRACT;
		case SDLK_KP_MINUS:            return Input::KEY_KP_MINUS;
		case SDLK_KP_MULTIPLY:         return Input::KEY_KP_MULTIPLY;
		case SDLK_KP_OCTAL:            return Input::KEY_KP_OCTAL;
		case SDLK_KP_PERCENT:          return Input::KEY_KP_PERCENT;
		case SDLK_KP_PERIOD:           return Input::KEY_KP_PERIOD;
		case SDLK_KP_PLUS:             return Input::KEY_KP_PLUS;
		case SDLK_KP_PLUSMINUS:        return Input::KEY_KP_PLUSMINUS;
		case SDLK_KP_POWER:            return Input::KEY_KP_POWER;
		case SDLK_KP_RIGHTBRACE:       return Input::KEY_KP_RIGHTBRACE;
		case SDLK_KP_RIGHTPAREN:       return Input::KEY_KP_RIGHTPAREN;
		case SDLK_KP_SPACE:            return Input::KEY_KP_SPACE;
		case SDLK_KP_TAB:              return Input::KEY_KP_TAB;
		case SDLK_KP_VERTICALBAR:      return Input::KEY_KP_VERTICALBAR;
		case SDLK_KP_XOR:              return Input::KEY_KP_XOR;
		case SDLK_l:                   return Input::KEY_L;
		case SDLK_LALT:                return Input::KEY_LALT;
		case SDLK_LCTRL:               return Input::KEY_LCTRL;
		case SDLK_LEFT:                return Input::KEY_LEFT;
		case SDLK_LEFTBRACKET:         return Input::KEY_LEFTBRACKET;
		case SDLK_LGUI:                return Input::KEY_LGUI;
		case SDLK_LSHIFT:              return Input::KEY_LSHIFT;
		case SDLK_m:                   return Input::KEY_M;
		case SDLK_MAIL:                return Input::KEY_MAIL;
		case SDLK_MEDIASELECT:         return Input::KEY_MEDIASELECT;
		case SDLK_MENU:                return Input::KEY_MENU;
		case SDLK_MINUS:               return Input::KEY_MINUS;
		case SDLK_MODE:                return Input::KEY_MODE;
		case SDLK_MUTE:                return Input::KEY_MUTE;
		case SDLK_n:                   return Input::KEY_N;
		case SDLK_NUMLOCKCLEAR:        return Input::KEY_NUMLOCKCLEAR;
		case SDLK_o:                   return Input::KEY_O;
		case SDLK_OPER:                return Input::KEY_OPER;
		case SDLK_OUT:                 return Input::KEY_OUT;
		case SDLK_p:                   return Input::KEY_P;
		case SDLK_PAGEDOWN:            return Input::KEY_PAGEDOWN;
		case SDLK_PAGEUP:              return Input::KEY_PAGEUP;
		case SDLK_PASTE:               return Input::KEY_PASTE;
		case SDLK_PAUSE:               return Input::KEY_PAUSE;
		case SDLK_PERIOD:              return Input::KEY_PERIOD;
		case SDLK_POWER:               return Input::KEY_POWER;
		case SDLK_PRINTSCREEN:         return Input::KEY_PRINTSCREEN;
		case SDLK_PRIOR:               return Input::KEY_PRIOR;
		case SDLK_q:                   return Input::KEY_Q;
		case SDLK_r:                   return Input::KEY_R;
		case SDLK_RALT:                return Input::KEY_RALT;
		case SDLK_RCTRL:               return Input::KEY_RCTRL;
		case SDLK_RETURN:              return Input::KEY_RETURN;
		case SDLK_RETURN2:             return Input::KEY_RETURN2;
		case SDLK_RGUI:                return Input::KEY_RGUI;
		case SDLK_RIGHT:               return Input::KEY_RIGHT;
		case SDLK_RIGHTBRACKET:        return Input::KEY_RIGHTBRACKET;
		case SDLK_RSHIFT:              return Input::KEY_RSHIFT;
		case SDLK_s:                   return Input::KEY_S;
		case SDLK_SCROLLLOCK:          return Input::KEY_SCROLLLOCK;
		case SDLK_SELECT:              return Input::KEY_SELECT;
		case SDLK_SEMICOLON:           return Input::KEY_SEMICOLON;
		case SDLK_SEPARATOR:           return Input::KEY_SEPARATOR;
		case SDLK_SLASH:               return Input::KEY_SLASH;
		case SDLK_SLEEP:               return Input::KEY_SLEEP;
		case SDLK_SPACE:               return Input::KEY_SPACE;
		case SDLK_STOP:                return Input::KEY_STOP;
		case SDLK_SYSREQ:              return Input::KEY_SYSREQ;
		case SDLK_t:                   return Input::KEY_T;
		case SDLK_TAB:                 return Input::KEY_TAB;
		case SDLK_THOUSANDSSEPARATOR:  return Input::KEY_THOUSANDSSEPARATOR;
		case SDLK_u:                   return Input::KEY_U;
		case SDLK_UNDO:                return Input::KEY_UNDO;
		case SDLK_UNKNOWN:             return Input::KEY_UNKNOWN;
		case SDLK_UP:                  return Input::KEY_UP;
		case SDLK_v:                   return Input::KEY_V;
		case SDLK_VOLUMEDOWN:          return Input::KEY_VOLUMEDOWN;
		case SDLK_VOLUMEUP:            return Input::KEY_VOLUMEUP;
		case SDLK_w:                   return Input::KEY_W;
		case SDLK_WWW:                 return Input::KEY_WWW;
		case SDLK_x:                   return Input::KEY_X;
		case SDLK_y:                   return Input::KEY_Y;
		case SDLK_z:                   return Input::KEY_Z;
		case SDLK_AMPERSAND:           return Input::KEY_AMPERSAND;
		case SDLK_ASTERISK:            return Input::KEY_ASTERISK;
		case SDLK_AT:                  return Input::KEY_AT;
		case SDLK_CARET:               return Input::KEY_CARET;
		case SDLK_COLON:               return Input::KEY_COLON;
		case SDLK_DOLLAR:              return Input::KEY_DOLLAR;
		case SDLK_EXCLAIM:             return Input::KEY_EXCLAIM;
		case SDLK_GREATER:             return Input::KEY_GREATER;
		case SDLK_HASH:                return Input::KEY_HASH;
		case SDLK_LEFTPAREN:           return Input::KEY_LEFTPAREN;
		case SDLK_LESS:                return Input::KEY_LESS;
		case SDLK_PERCENT:             return Input::KEY_PERCENT;
		case SDLK_PLUS:                return Input::KEY_PLUS;
		case SDLK_QUESTION:            return Input::KEY_QUESTION;
		case SDLK_QUOTEDBL:            return Input::KEY_QUOTEDBL;
		case SDLK_RIGHTPAREN:          return Input::KEY_RIGHTPAREN;
		case SDLK_UNDERSCORE:          return Input::KEY_UNDERSCORE;

		// Rename SDL Mouse Keycodes
		case SDL_BUTTON_LEFT:          return Input::KEY_MOUSELEFT;
		case SDL_BUTTON_MIDDLE:        return Input::KEY_MOUSEMIDDLE;
		case SDL_BUTTON_RIGHT:         return Input::KEY_MOUSERIGHT;
	}

	return Input::KEY_UNKNOWN;
}

void Input::checkSdlEvents() {
	while (SDL_PollEvent(&sdlEvent)) {
		switch (sdlEvent.type) {
			case SDL_QUIT:
				engine->stop();
				break;

			case SDL_KEYDOWN:
				if (listener != nullptr) {
					Input::Keycode keycode;
					keycode = translateSdlKeycode(sdlEvent.key.keysym.sym);
					listener->keyDown(keycode);
				}
				break;

			case SDL_KEYUP:
				if (listener != nullptr) {
					Input::Keycode keycode;
					keycode = translateSdlKeycode(sdlEvent.key.keysym.sym);
					listener->keyUp(keycode);
				}
				break;

			case SDL_MOUSEBUTTONDOWN:
				if (listener != nullptr) {
					Input::Keycode keycode;
					keycode = translateSdlKeycode(sdlEvent.button.button);
					listener->mouseDown(keycode,
							Vector2(sdlEvent.button.x, sdlEvent.button.y));
				}
				break;

			case SDL_MOUSEBUTTONUP:
				if (listener != nullptr) {
					Input::Keycode keycode;
					keycode = translateSdlKeycode(sdlEvent.button.button);
					listener->mouseUp(keycode,
							Vector2(sdlEvent.button.x, sdlEvent.button.y));
				}
				break;

		}
	}
}

InputListener::~InputListener() { }

} // namespace jorge

