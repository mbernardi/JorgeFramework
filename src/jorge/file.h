#ifndef JORGE_FILE_H
#define JORGE_FILE_H

#include <string>
#include <cstdint>

#include <SDL.h>

namespace jorge {

class Path {
	public:
		/**
		 * Creates a absolute path from a path relative to resources folder
		 * Assumes folder structure as follows:
		 *
		 * root/
		 *     bin/
		 *         (Binary)
		 *     res/
		 *         (Resources)
		 *
		 * @param relativePath Relative path from resources folder to file
		 */
		Path(std::string relativePath);

		/**
		 * Get absolute path C string
		 */
		const char* get();

		/**
		 * Get absolute path std::string
		 */
		std::string getString();

	private:
		static std::string resourcesPath;

		std::string absolutePath;
};

class File {
	public:
		enum Operation {FILE_NEW_RW,
		                FILE_OPEN_R,
		                FILE_OPEN_RW,
		                FILE_OPEN_RA};

		enum SeekWhence {FILE_SEEK_WHENCE_START,
		                 FILE_SEEK_WHENCE_CURRENT,
						 FILE_SEEK_WHENCE_END};

		/**
		 * Open or create a file for reading or writing
		 * Allowed operations are:
		 *     FILE_NEW_RW: Creates (or overwrites) a file for reading and
		 *                  writing
		 *     FILE_OPEN_R: Open an existing file for reading
		 *     FILE_OPEN_RW: Open an existing file for reading and writing
		 *     FILE_OPEN_RA: Open an existing file for reading and appending
		 *                   You can seek the pointer anywhere in the file for
		 *                   reading but writing will move it back to the end
		 *                   of the file
		 * Throws FileOpenException if cant open file
		 * @param filePath Path to file
		 * @param operation
		 */
		File(Path filePath, Operation operation);

		/**
		 * Writes data to file
		 * @param data Address to data
		 * @param size Length of object to write (use sizeof)
		 * @param number Number of objects to write (default is 1)
		 * Throws FileOperationException if you can't write to that file
		 * Throws FileWriteException if write failed containing number of
		 * objects written
		 */
		void write(const void* data, std::size_t size, int number = 1);

		/**
		 * Reads data from file
		 * @param data Address where to store data read
		 * @param size Length of object to read (use sizeof)
		 * @param number Number of objects to read (default is 1)
		 * Throws FileReadException if reached EOF or if read failed, containing
		 * number of objects read, being 0 if reached EOF or if an error ocurred
		 */
		void read(void* data, std::size_t size, int number = 1);

		/**
		 * Sets read/write position in file
		 * @param offset Offset in bytes relative to whence location, can be
		 * negative
		 * @param whence Can be FILE_SEEK_WHENCE_START, FILE_SEEK_WHENCE_CURRENT
		 * or FILE_SEEK_WHENCE_END
		 * Throws FileSeekException if an error ocurred
		 */
		void seek(int64_t offset, SeekWhence whence);

		/**
		 * Returns current position in file
		 */
		int64_t getPosition();

		/**
		 * Returns size of file
		 */
		int64_t getSize();

		~File();

	private:
		SDL_RWops* sdlFile;
		Operation operation;
};

} // namespace jorge

#endif
