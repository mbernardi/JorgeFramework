#include "game.h"

#include "screen.h"
#include "engine.h"
#include "exception.h"
#include "log.h"

namespace jorge {

void Game::start(Engine* engine) {
	this->engine = engine;
	currentScreen = nullptr;
}

void Game::loop(double& dt) {
	if (currentScreen != nullptr) {
		currentScreen->loop(dt);
	}
}

void Game::stop() {
	if (currentScreen != nullptr && !currentScreen->isHidden()) {
		currentScreen->hide();
	}
	if (currentScreen != nullptr && !currentScreen->isStopped()) {
		currentScreen->stop();
	}
}

void Game::quit() {
	engine->stop();
}

void Game::setScreen(Screen* screen, bool stop) {
	// Check argument
	if (screen == nullptr) {
		throw InvalidArgumentException("Game::setScreen()",
				"Passed nullptr as screen");
	}

	// Stop/Hide previous screen
	if (currentScreen != nullptr) {
		currentScreen->hide();
		if (stop) {
			currentScreen->stop();
		}
	}

	// Start/Show new screen
	currentScreen = screen;
	if (currentScreen->isStopped()) {
		currentScreen->start();
	}
	currentScreen->show();
}

Screen* Game::getScreen() {
	return currentScreen;
}

Engine* Game::getEngine() {
	return engine;
}

Game::~Game() {

}

} // namespace jorge

