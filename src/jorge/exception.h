#ifndef JORGE_EXCEPTION_H
#define JORGE_EXCEPTION_H

#include <stdexcept>
#include <string>

namespace jorge {

/**
 * Framework-related problem, unrecoverable
 */
class JorgeError : public std::runtime_error {
	public:
		/**
		 * @param message
		 */
		JorgeError(std::string message);
};

/**
 * SDL-related problem, unrecoverable
 */
class SdlError : public std::runtime_error {
	public:
		/**
		 * @param message
		 */
		SdlError(std::string message);
};

/**
 * Game-related problem, unrecoverable
 */
class GameError : public std::runtime_error {
	public:
		/**
		 * @param message
		 */
		GameError(std::string message);
};

/**
 * Framework-related problem, less severe
 */
class JorgeException : public std::runtime_error {
	public:
		/**
		 * @param message
		 */
		JorgeException(std::string message);

		/**
		 * @param function Where the exception ocurred, e.g. Sound::play()
		 * @param message
		 */
		JorgeException(std::string function, std::string message);
};

/**
 * Thrown when calling Music::play() when music is already playing,
 * Music::pause() when music is already paused, etc.
 */
class InvalidMusicOperationException : public JorgeException {
	public:
		using JorgeException::JorgeException; // Inherit constructors (C++11)
};

class InvalidArgumentException : public JorgeException {
	public:
		using JorgeException::JorgeException; // Inherit constructors (C++11)
};

class FileOpenException : public JorgeException {
	public:
		using JorgeException::JorgeException; // Inherit constructors (C++11)
};

class FileOperationException : public JorgeException {
	public:
		using JorgeException::JorgeException; // Inherit constructors (C++11)
};

class FileWriteException : public JorgeException {
	public:
		FileWriteException(std::string function, std::string message, int success);

		/**
		 * Returns number of objects writtem
		 */
		int getSuccess();

	protected:
		int success;
};

class FileReadException : public JorgeException {
	public:
		FileReadException(std::string function, std::string message, int success);

		/**
		 * Returns number of objects read, being 0 if reached EOF or if an error
		 * ocurred
		 */
		int getSuccess();

	protected:
		int success;
};

class FileSeekException : public JorgeException {
	public:
		using JorgeException::JorgeException; // Inherit constructors (C++11)
};


} //namespace jorge

#endif
