#ifndef JORGE_GRAPHICS_H
#define JORGE_GRAPHICS_H

#include <cstdint>

#include <SDL.h>

#include "engine.h"
#include "file.h"
#include "math.h"

namespace jorge {

class Engine; // Because of circular dependency

/**
 * Defines an RGBA color
 */
class Color {
	public:
		/**
		 * Creates a black transparent color
		 */
		Color();

		/**
		 * Creates a color, use numbers from 0 to 255
		 * @param r Red
		 * @param g Green
		 * @param b Blue
		 * @param a Alpha
		 */
		Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

		uint8_t getR();

		uint8_t getG();

		uint8_t getB();

		uint8_t getA();

		/**
		 * You should'nt call this
		 * Used by
		 */
		SDL_Color getSdlColor();

	private:
		SDL_Color sdlColor;
};

/**
 * Contains static graphics related functions
 */
class Graphics {
	public:
		static void setIcon(Engine* engine);

		static void setIcon(Engine* engine, Path iconPath);

		/**
		 * Set drawing color
		 * @param engine
		 * @param color
		 */
		static void setDrawColor(Engine* engine, Color color);

		/**
		 * Draw a rectangle
		 * Previously set color with setDrawColor
		 * @param engine
		 * @param rect
		 * @param filled Set to true if you want a filled rectangle, true by
		 * default
		 */
		static void drawRectangle(Engine* engine, Rectangle rect,
				bool filled = false);

		/**
		 * Draw a line between two points
		 * Previously set color with setDrawColor
		 * @param engine
		 * @param vect1 First point
		 * @param vect2 Second point
		 */
		static void drawLine(Engine* engine, Vector2 vect1, Vector2 vect2);

		/**
		 * Draw a point
		 * Previously set color with setDrawColor
		 * @param engine
		 * @param point First point
		 */
		static void drawPoint(Engine* engine, Vector2 point);

		/**
		 * You should'nt call this
		 * Used by the engine
		 */
		static void drawStart(SDL_Renderer* renderer);

		/**
		 * You should'nt call this
		 * Used by the engine
		 */
		static void drawFinish(SDL_Renderer* renderer);
};

} // namespace jorge

#endif
