#include "file.h"

#include <string>
#include <cstdint>

#include <SDL.h>

#include "log.h"
#include "exception.h"

namespace jorge {

// Initialize static variable, why I do need to do this C++??? Why???
std::string Path::resourcesPath;

Path::Path(std::string relativePath) {
	// Choose correct path separator
#ifdef _WIN32
	const char PATH_SEP = '\\';
#else
	const char PATH_SEP = '/';
#endif
	// Check if we already know the resources path, otherwise ask SDL
	if (resourcesPath.empty()) {
		/*
		 * Ask SDL for our current path
		 * SDL_GetBasePath will return NULL if something went wrong when
		 * retrieving the path
		 */
		char *basePath = SDL_GetBasePath();
		if (basePath) {
			resourcesPath = basePath;
			SDL_free(basePath);
		}
		else {
			std::string sdlError = SDL_GetError();
			throw SdlError("Cannot SDL_GetBasePath " + sdlError);
		}
		// Replace the last bin/ with res/ to get the the resource path
		size_t pos = resourcesPath.rfind("bin");
		resourcesPath = resourcesPath.substr(0, pos) + "res" + PATH_SEP;
	}

	// Append file path
	absolutePath = resourcesPath + relativePath;
}

const char* Path::get() {
	return absolutePath.c_str();
}

std::string Path::getString() {
	return absolutePath;
}

File::File(Path filePath, Operation operation)
		: operation(operation) {

	switch (operation) {
		case FILE_NEW_RW:
			sdlFile = SDL_RWFromFile(filePath.get(), "w+");
			break;

		case FILE_OPEN_R:
			sdlFile = SDL_RWFromFile(filePath.get(), "r");
			break;

		case FILE_OPEN_RW:
			sdlFile = SDL_RWFromFile(filePath.get(), "r+");
			break;

		case FILE_OPEN_RA:
			sdlFile = SDL_RWFromFile(filePath.get(), "a+");
			break;
	}
	if (sdlFile == NULL) {
		std::string sdlError = SDL_GetError();
		throw FileOpenException("File::File", "Cannot open/create file " +
				filePath.getString() + ", " + sdlError);
	}
}

void File::write(const void* data, std::size_t size, int number) {
	if (operation == File::FILE_NEW_RW ||
	    operation == File::FILE_OPEN_RW ||
	    operation == File::FILE_OPEN_RA) {

		int success = SDL_RWwrite(sdlFile, data, size, number);

		if (success != number) {
			std::string sdlError = SDL_GetError();
			throw FileWriteException("File::write",
					"Cannot complete write, " + sdlError, success);
		}
	}
	else {
		throw FileOperationException("File::write",
				"File opened as read only");
	}
}

void File::read(void* data, std::size_t size, int number) {

	int success = SDL_RWread(sdlFile, data, size, number);

	if (success == 0) {
		std::string sdlError = SDL_GetError();
		throw FileReadException("File::read",
				"Cannot read data or reached EOF, " + sdlError, success);
	}
	else if (success != number) {
		std::string sdlError = SDL_GetError();
		throw FileReadException("File::read",
				"Cannot complete read, " + sdlError, success);
	}
}

void File::seek(int64_t offset, SeekWhence whence) {
	int success;
	switch (whence) {
		case File::FILE_SEEK_WHENCE_START:
			success = SDL_RWseek(sdlFile, offset, RW_SEEK_SET);
			break;

		case File::FILE_SEEK_WHENCE_CURRENT:
			success = SDL_RWseek(sdlFile, offset, RW_SEEK_CUR);
			break;

		case File::FILE_SEEK_WHENCE_END:
			success = SDL_RWseek(sdlFile, offset, RW_SEEK_END);
			break;
	}

	if (success == -1) {
		std::string sdlError = SDL_GetError();
		throw FileSeekException("File::seek", "An error ocurred, " + sdlError);
	}
}

int64_t File::getPosition() {
	return SDL_RWtell(sdlFile);
}

int64_t File::getSize() {
	int64_t previousPosition = getPosition();
	seek(0, FILE_SEEK_WHENCE_END); // Go to end of file
	int64_t fileSize = getPosition();
	seek(previousPosition, File::FILE_SEEK_WHENCE_START); // Return
	return fileSize;
}

File::~File() {
	int success = SDL_RWclose(sdlFile);
	if (success != 0) {
		std::string sdlError = SDL_GetError();
		jorge::Log::e("Failed to close file, " + sdlError);
	}
}

} // namespace jorge
