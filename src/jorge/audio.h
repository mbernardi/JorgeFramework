#ifndef JORGE_AUDIO_H
#define JORGE_AUDIO_H

#include <string>

#include <SDL_mixer.h>

#include "file.h"

namespace jorge {

class Sound {
	public:
		/**
		 * Create sound from file
		 * Supports WAV, AIFF, RIFF, OGG and VOC
		 * @param soundPath Path to file
		 */
		Sound(Path soundPath);

		/**
		 * Play the sound once
		 */
		void play();

		/**
		 * Play the sound repeatedly
		 * Throws InvalidArgumentException if repeat argument is less than 1
		 * @param repeat Number of times
		 */
		void play(int repeat);

		~Sound();

	private:
		Mix_Chunk* sdlSound = nullptr;
};

/**
 * Background music
 * You can only play one music at once, that's why pause(), stop(), resume(),
 * isPlaying(), etc. are static
 */
class Music {
	public:

		/**
		 * Create music from file
		 * Supports WAV, MIDI, MOD, OGG, MP3, FLAC
		 * @param musicPath Path to file
		 */
		Music(Path musicPath);

		/**
		 * Play looping the song forever
		 * Starts from the beginning, music must be already stopped
		 * Throws InvalidMusicOperationException if music isn't already stopped
		 * Throws InvalidArgumentException if a nullptr is given as music
		 * @param music Song to play
		 */
		static void play(Music* music);

		/**
		 * Play repeatedly
		 * Starts from the beginning, music must be already stopped
		 * Throws InvalidArgumentException if repeat argument is less than 0
		 * Throws InvalidMusicOperationException if music isn't already stopped
		 * Throws InvalidArgumentException if a nullptr is given as music
		 * @param music Song to play
		 * @param repeat Number of times, 0 is forever
		 */
		static void play(Music* music, int repeat);

		/**
		 * Pauses the song, music must be already playing
		 * Throws InvalidMusicOperationException if music isn't already playing
		 */
		static void pause();

		/**
		 * Resumes a paused song, music must be already paused
		 * Throws InvalidMusicOperationException if music isn't already paused
		 */
		static void resume();

		/**
		 * Stops the song, music must be playing or paused
		 * Throws InvalidMusicOperationException if music is already stopped
		 */
		static void stop();

		static bool isPlaying();

		/**
		 * Returns true only when the music is paused
		 * When the music is stopped returns false
		 */
		static bool isPaused();

		/**
		 * Returns true only when the music is stopped
		 * When the music is paused returns false
		 */
		static bool isStopped();

		~Music();

	private:
		Mix_Music* sdlMusic = nullptr;
};

} // namespace jorge

#endif
