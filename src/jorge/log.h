#ifndef JORGE_LOG_H
#define JORGE_LOG_H

#include <string>

namespace jorge {

/**
 * Contains static logging-related functions
 */
class Log {
	public:
		static bool debug; // Specify if printing debug messages

		/**
		 * Prints runtime errors on terminal
		 * @param message
		 */
		static void e(const std::string &message);

		/**
		 * Prints warnings on terminal
		 * @param message
		 */
		static void w(const std::string &message);

		/**
		 * Prints information on terminal
		 * @param message
		 */
		static void i(const std::string &message);

		/**
		 * Prints debug information on terminal (only when enabled)
		 * @param message
		 */
		static void d(const std::string &message);
};

} // namespace jorge

#endif
