#ifndef JORGE_INPUT_H
#define JORGE_INPUT_H

#include <SDL.h>

#include "math.h"
#include "engine.h"

namespace jorge {

class InputListener; // Because of circular dependency
class Engine; // Because of circular dependency

class Input {
	public:
		enum Keycode {
			KEY_0,                       KEY_1,
			KEY_2,                       KEY_3,
			KEY_4,                       KEY_5,
			KEY_6,                       KEY_7,
			KEY_8,                       KEY_9,
			KEY_A,                       KEY_AC_BACK,
			KEY_AC_BOOKMARKS,            KEY_AC_FORWARD,
			KEY_AC_HOME,                 KEY_AC_REFRESH,
			KEY_AC_SEARCH,               KEY_AC_STOP,
			KEY_AGAIN,                   KEY_ALTERASE,
			KEY_QUOTE,                   KEY_APPLICATION,
			KEY_AUDIOMUTE,               KEY_AUDIONEXT,
			KEY_AUDIOPLAY,               KEY_AUDIOPREV,
			KEY_AUDIOSTOP,               KEY_B,
			KEY_BACKSLASH,               KEY_BACKSPACE,
			KEY_BRIGHTNESSDOWN,          KEY_BRIGHTNESSUP,
			KEY_C,                       KEY_CALCULATOR,
			KEY_CANCEL,                  KEY_CAPSLOCK,
			KEY_CLEAR,                   KEY_CLEARAGAIN,
			KEY_COMMA,                   KEY_COMPUTER,
			KEY_COPY,                    KEY_CRSEL,
			KEY_CURRENCYSUBUNIT,         KEY_CURRENCYUNIT,
			KEY_CUT,                     KEY_D,
			KEY_DECIMALSEPARATOR,        KEY_DELETE,
			KEY_DISPLAYSWITCH,           KEY_DOWN,
			KEY_E,                       KEY_EJECT,
			KEY_END,                     KEY_EQUALS,
			KEY_ESCAPE,                  KEY_EXECUTE,
			KEY_EXSEL,                   KEY_F,
			KEY_F1,                      KEY_F10,
			KEY_F11,                     KEY_F12,
			KEY_F13,                     KEY_F14,
			KEY_F15,                     KEY_F16,
			KEY_F17,                     KEY_F18,
			KEY_F19,                     KEY_F2,
			KEY_F20,                     KEY_F21,
			KEY_F22,                     KEY_F23,
			KEY_F24,                     KEY_F3,
			KEY_F4,                      KEY_F5,
			KEY_F6,                      KEY_F7,
			KEY_F8,                      KEY_F9,
			KEY_FIND,                    KEY_G,
			KEY_BACKQUOTE,               KEY_H,
			KEY_HELP,                    KEY_HOME,
			KEY_I,                       KEY_INSERT,
			KEY_J,                       KEY_K,
			KEY_KBDILLUMDOWN,            KEY_KBDILLUMTOGGLE,
			KEY_KBDILLUMUP,              KEY_KP_0,
			KEY_KP_00,                   KEY_KP_000,
			KEY_KP_1,                    KEY_KP_2,
			KEY_KP_3,                    KEY_KP_4,
			KEY_KP_5,                    KEY_KP_6,
			KEY_KP_7,                    KEY_KP_8,
			KEY_KP_9,                    KEY_KP_A,
			KEY_KP_AMPERSAND,            KEY_KP_AT,
			KEY_KP_B,                    KEY_KP_BACKSPACE,
			KEY_KP_BINARY,               KEY_KP_C,
			KEY_KP_CLEAR,                KEY_KP_CLEARENTRY,
			KEY_KP_COLON,                KEY_KP_COMMA,
			KEY_KP_D,                    KEY_KP_DBLAMPERSAND,
			KEY_KP_DBLVERTICALBAR,       KEY_KP_DECIMAL,
			KEY_KP_DIVIDE,               KEY_KP_E,
			KEY_KP_ENTER,                KEY_KP_EQUALS,
			KEY_KP_EQUALSAS400,          KEY_KP_EXCLAM,
			KEY_KP_F,                    KEY_KP_GREATER,
			KEY_KP_HASH,                 KEY_KP_HEXADECIMAL,
			KEY_KP_LEFTBRACE,            KEY_KP_LEFTPAREN,
			KEY_KP_LESS,                 KEY_KP_MEMADD,
			KEY_KP_MEMCLEAR,             KEY_KP_MEMDIVIDE,
			KEY_KP_MEMMULTIPLY,          KEY_KP_MEMRECALL,
			KEY_KP_MEMSTORE,             KEY_KP_MEMSUBTRACT,
			KEY_KP_MINUS,                KEY_KP_MULTIPLY,
			KEY_KP_OCTAL,                KEY_KP_PERCENT,
			KEY_KP_PERIOD,               KEY_KP_PLUS,
			KEY_KP_PLUSMINUS,            KEY_KP_POWER,
			KEY_KP_RIGHTBRACE,           KEY_KP_RIGHTPAREN,
			KEY_KP_SPACE,                KEY_KP_TAB,
			KEY_KP_VERTICALBAR,          KEY_KP_XOR,
			KEY_L,                       KEY_LALT,
			KEY_LCTRL,                   KEY_LEFT,
			KEY_LEFTBRACKET,             KEY_LGUI,
			KEY_LSHIFT,                  KEY_M,
			KEY_MAIL,                    KEY_MEDIASELECT,
			KEY_MENU,                    KEY_MINUS,
			KEY_MODE,                    KEY_MUTE,
			KEY_N,                       KEY_NUMLOCKCLEAR,
			KEY_O,                       KEY_OPER,
			KEY_OUT,                     KEY_P,
			KEY_PAGEDOWN,                KEY_PAGEUP,
			KEY_PASTE,                   KEY_PAUSE,
			KEY_PERIOD,                  KEY_POWER,
			KEY_PRINTSCREEN,             KEY_PRIOR,
			KEY_Q,                       KEY_R,
			KEY_RALT,                    KEY_RCTRL,
			KEY_RETURN,                  KEY_RETURN2,
			KEY_RGUI,                    KEY_RIGHT,
			KEY_RIGHTBRACKET,            KEY_RSHIFT,
			KEY_S,                       KEY_SCROLLLOCK,
			KEY_SELECT,                  KEY_SEMICOLON,
			KEY_SEPARATOR,               KEY_SLASH,
			KEY_SLEEP,                   KEY_SPACE,
			KEY_STOP,                    KEY_SYSREQ,
			KEY_T,                       KEY_TAB,
			KEY_THOUSANDSSEPARATOR,      KEY_U,
			KEY_UNDO,                    KEY_UNKNOWN,
			KEY_UP,                      KEY_V,
			KEY_VOLUMEDOWN,              KEY_VOLUMEUP,
			KEY_W,                       KEY_WWW,
			KEY_X,                       KEY_Y,
			KEY_Z,                       KEY_AMPERSAND,
			KEY_ASTERISK,                KEY_AT,
			KEY_CARET,                   KEY_COLON,
			KEY_DOLLAR,                  KEY_EXCLAIM,
			KEY_GREATER,                 KEY_HASH,
			KEY_LEFTPAREN,               KEY_LESS,
			KEY_PERCENT,                 KEY_PLUS,
			KEY_QUESTION,                KEY_QUOTEDBL,
			KEY_RIGHTPAREN,              KEY_UNDERSCORE,

			KEY_MOUSELEFT,               KEY_MOUSEMIDDLE,
			KEY_MOUSERIGHT,
		};

		Input(Engine* engine);

		/**
		 * Returns mouse position on screen
		 */
		Vector2 getMousePosition();

		void setInputListener(InputListener* listener);

		/**
		 * You should'nt call this
		 * Used by the engine
		 */
		void checkSdlEvents();

	private:
		SDL_Event sdlEvent;

		Engine* engine = nullptr;

		InputListener* listener = nullptr;

		Keycode translateSdlKeycode(SDL_Keycode code);
};

class InputListener {
	public:
		virtual void keyDown(Input::Keycode key) = 0;

		virtual void keyUp(Input::Keycode key) = 0;

		virtual void mouseDown(Input::Keycode key, Vector2 pos) = 0;

		virtual void mouseUp(Input::Keycode key, Vector2 pos) = 0;

		virtual ~InputListener();

};

} // namespace jorge

#endif
