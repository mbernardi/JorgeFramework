#ifndef GAME_H
#define GAME_H

#include <string>

#include <jorge/engine.h>
#include <jorge/game.h>
#include <jorge/texture.h>
#include <jorge/resourcesmanager.h>

#include "mainscreen/mainscreen.h"
#include "shapesscreen/shapesscreen.h"

class MainScreen; // Because of circular includes
class ShapesScreen; // Because of circular includes

/**
 * Main game class.
 * Inherits loop() and start() from GameInterface
 */
class ExampleGame : public jorge::Game {
	public:
		MainScreen* mainScreen = nullptr;
		ShapesScreen* shapesScreen = nullptr;

		ExampleGame(std::string version);

		void start(jorge::Engine*);

		void loop(double& dt);

		void stop();

		std::string getVersion();

		~ExampleGame();

	private:
		std::string version;

		jorge::ResourcesManager* res = nullptr;

		/**
		 * Loads textures, sounds and music into a ResourcesManager
		 */
		void loadResources();

		/**
		 * Reads a text file and returns a string
		 */
		std::string readTextFile(jorge::Path filePath);

		/**
		 * Returns the number of times this game was opened and adds one to
		 * the file
		 */
		int gameOpenCount();
};

#endif

