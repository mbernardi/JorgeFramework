#include "examplegame.h"

#include <string>
#include <vector>

#include <jorge/texture.h>
#include <jorge/engine.h>
#include <jorge/math.h>
#include <jorge/log.h>
#include <jorge/file.h>
#include <jorge/exception.h>
#include <jorge/resourcesmanager.h>

#include "mainscreen/mainscreen.h"
#include "shapesscreen/shapesscreen.h"

ExampleGame::ExampleGame(std::string version) : version(version) {
	jorge::Log::i("Version: " + version);
}

void ExampleGame::start(jorge::Engine* engine) {
	jorge::Log::d("ExampleGame start()");
	Game::start(engine);

	res = new jorge::ResourcesManager();

	loadResources();

	// Create screens
	mainScreen = new MainScreen(engine, this, res);
	shapesScreen = new ShapesScreen(engine, this, res);

	// Start screen
	setScreen(mainScreen);
}

void ExampleGame::loop(double& dt) {
	Game::loop(dt);
}

void ExampleGame::stop() {
	jorge::Log::d("ExampleGame stop()");
	Game::stop();
}

ExampleGame::~ExampleGame() {
	jorge::Log::d("ExampleGame delete");
	delete mainScreen;
	delete shapesScreen;
	delete res;
}

std::string ExampleGame::getVersion() {
	return version;
}

void ExampleGame::loadResources() {
	/*
	 * For simple textures you can create a unordered_map with strings as keys
	 * and jorge::Paths as values, then ResourcesManager reads the map and loads
	 * the textures
	 */
	std::unordered_map<std::string, jorge::Path> paths;

	paths.insert(std::make_pair("background", jorge::Path("background.png")));
	paths.insert(std::make_pair("title", jorge::Path("jorge.png")));
	paths.insert(std::make_pair("ball", jorge::Path("ball.png")));
	paths.insert(std::make_pair("circleNormal", jorge::Path("circleNormal.png")));
	paths.insert(std::make_pair("circleColliding",
				jorge::Path("circleColliding.png")));

	res->loadTextures(engine, paths);

	// Text
	// Load and update files (instructions text is in instructions.txt
	std::string instructions = readTextFile(jorge::Path("instructions.txt"));
	int count = gameOpenCount(); // Number of times game was opened

	res->addTexture("text",
			new jorge::TextTexture(engine, "JorgeFramework",
			jorge::Path("LiberationSans-Regular.ttf"),
			jorge::Color(0, 0, 0, 255), 50));

	res->addTexture("instructions",
			new jorge::TextTexture(engine, instructions + std::to_string(count),
			jorge::Path("LiberationSans-Regular.ttf"),
			jorge::Color(0, 0, 0, 150), 10, 200));

	// Sounds and music
	res->addMusic("mainMusic", new jorge::Music(jorge::Path("music.wav")));
	res->addSound("mainSound", new jorge::Sound(jorge::Path("sound.ogg")));
}

std::string ExampleGame::readTextFile(jorge::Path filePath) {
	jorge::File file = jorge::File(filePath, jorge::File::FILE_OPEN_RW);

	std::vector<char> textArray(file.getSize());

	file.read(textArray.data(), sizeof(char), file.getSize());

	return std::string(textArray.begin(), textArray.end());
}

int ExampleGame::gameOpenCount() {
	jorge::File* file;
	int count;
	try {
		file = new jorge::File(jorge::Path("gameOpenCount"),
				jorge::File::FILE_OPEN_RW);

		file->read(&count, sizeof(int)); // Changes int count
	}
	catch (jorge::FileOpenException e) { // Means file doesn't exist
		file = new jorge::File(jorge::Path("gameOpenCount"),
				jorge::File::FILE_NEW_RW);

		count = 0;
	}

	int newCount = count + 1;

	// Go back to start of file in case file was read
	file->seek(0, jorge::File::FILE_SEEK_WHENCE_START);

	file->write(&newCount, sizeof(int));

	delete file;

	return newCount;
}
