#include "inputmanager.h"

#include <jorge.h>

#include "ball.h"
#include "mainscreen.h"

InputManager::InputManager(MainScreen* screen) : screen(screen) { }

void InputManager::keyDown(jorge::Input::Keycode key) {
	switch (key) {
		// Move ball to the right
		case jorge::Input::KEY_RIGHT:
			screen->ball->setMovingRight(true);
			break;

		// Move ball to the left
		case jorge::Input::KEY_LEFT:
			screen->ball->setMovingLeft(true);
			break;

		// Play music
		case jorge::Input::KEY_A:
			jorge::Log::d("Playing music");
			try {
				jorge::Music::play(screen->res->getMusic("mainMusic"));
			}
			catch (jorge::InvalidMusicOperationException e){
				// When music isn't stopped
				jorge::Log::w(e.what());
			}
			break;

		// Pause the music
		case jorge::Input::KEY_P:
			jorge::Log::d("Pausing music");
			try {
				jorge::Music::pause();
			}
			catch (jorge::InvalidMusicOperationException e){
				// When music isn't playing
				jorge::Log::w(e.what());
			}
			break;

		// Resume the music
		case jorge::Input::KEY_R:
			jorge::Log::d("Resuming music");
			try {
				jorge::Music::resume();
			}
			catch (jorge::InvalidMusicOperationException e){
				// When music isn't paused
				jorge::Log::w(e.what());
			}
			break;

		// Stop the music
		case jorge::Input::KEY_S:
			jorge::Log::d("Stopping music");
			try {
				jorge::Music::stop();
			}
			catch (jorge::InvalidMusicOperationException e){
				// When music is already stopped
				jorge::Log::w(e.what());
			}
			break;

		// Play a sound
		case jorge::Input::KEY_F:
			jorge::Log::d("Playing sound");
			screen->res->getSound("mainSound")->play();
			break;

		// Go to ShapesScreen
		case jorge::Input::KEY_M:
			screen->goToShapesScreen();
			break;

		default:
			jorge::Log::d("Pressed Key does nothing");
			break;
	}
}

void InputManager::keyUp(jorge::Input::Keycode key) {
	switch (key) {
		// Move ball to the right
		case jorge::Input::KEY_RIGHT:
			screen->ball->setMovingRight(false);
			break;

		// Move ball to the right
		case jorge::Input::KEY_LEFT:
			screen->ball->setMovingLeft(false);
			break;

		default:
			break;
	}
}

void InputManager::mouseDown(jorge::Input::Keycode key, jorge::Vector2 pos) {
	switch (key) {
		case jorge::Input::KEY_MOUSELEFT:
			jorge::Log::i("Mouse left down at " + pos.toString());
			break;

		case jorge::Input::KEY_MOUSEMIDDLE:
			jorge::Log::i("Mouse middle down at " + pos.toString());
			break;

		case jorge::Input::KEY_MOUSERIGHT:
			jorge::Log::i("Mouse right down at " + pos.toString());
			break;

		default:
			break;
	}
}

void InputManager::mouseUp(jorge::Input::Keycode key, jorge::Vector2 pos) {
	switch (key) {
		case jorge::Input::KEY_MOUSELEFT:
			jorge::Log::i("Mouse left up at " + pos.toString());
			break;

		case jorge::Input::KEY_MOUSEMIDDLE:
			jorge::Log::i("Mouse middle up at " + pos.toString());
			break;

		case jorge::Input::KEY_MOUSERIGHT:
			jorge::Log::i("Mouse right up at " + pos.toString());
			break;

		default:
			break;
	}
}
