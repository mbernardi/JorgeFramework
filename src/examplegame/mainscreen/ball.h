#ifndef BALL_H
#define BALL_H

#include <jorge/math.h>

/**
 * A simple ball moving around
 */
class Ball {
	public:
		Ball(jorge::Vector2 pos);

		void update(float dt);

		/**
		 * If set to true, ball will move right
		 */
		void setMovingRight(bool status);

		/**
		 * If set to true, ball will move left
		 */
		void setMovingLeft(bool status);

		jorge::Vector2 getPosition();

	private:
		jorge::Vector2 pos;

		float speed; // px/s
		bool movingLeft;
		bool movingRight;
};

#endif
