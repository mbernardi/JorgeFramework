#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "mainscreen.h"

#include <jorge/input.h>
#include <jorge/math.h>

class MainScreen; // Because of circular includes

class InputManager: public jorge::InputListener {
	public:
		InputManager(MainScreen* screen);

		void keyDown(jorge::Input::Keycode key);

		void keyUp(jorge::Input::Keycode key);

		void mouseDown(jorge::Input::Keycode key, jorge::Vector2 pos);

		void mouseUp(jorge::Input::Keycode key, jorge::Vector2 pos);

	private:
		MainScreen* screen = nullptr;
};

#endif
