#include "mainscreen.h"

#include <string>

#include <jorge.h>

#include "ball.h"
#include "../examplegame.h"

MainScreen::MainScreen(jorge::Engine* engine, ExampleGame* game,
		jorge::ResourcesManager* res)
		: engine(engine), game(game), res(res) { }

void MainScreen::start() {
	jorge::Log::d("MainScreen start()");
	Screen::start();

	/*
	 * Set up input, input is managed by a separate object
	 * Now we create the object that manages the input, then in Screen::show()
	 * we tell jorge that we want to use it as the input listener
	 * We do that in show() because when returning to this screen start() is
	 * not called
	 */
	inputManager = new InputManager(this);

	// Create the moving ball
	ball = new Ball(jorge::Vector2(400, 100));

	// Sound
	res->getSound("mainSound")->play(3); // Play sound three times

	// Music
	jorge::Music::play(res->getMusic("mainMusic"));
	jorge::Log::d("Playing music");
}

void MainScreen::loop(double& dt) {

	// Background
	res->getTexture("background")->draw(engine,
			jorge::Rectangle(0, 0, 800, 600));

	// Title
	res->getTexture("title")->draw(engine,
			jorge::Vector2(400, 300), jorge::Rectangle::ALIGN_CENTER);

	// Text
	res->getTexture("text")->draw(engine,
			jorge::Vector2(400, 470), jorge::Rectangle::ALIGN_CENTER);

	res->getTexture("instructions")->draw(engine,
			jorge::Vector2(55, 55));

	// Ball
	ball->update(dt);

	res->getTexture("ball")->draw(engine,
			ball->getPosition(), jorge::Rectangle::ALIGN_CENTER);
}

void MainScreen::show() {
	jorge::Log::d("MainScreen show()");
	Screen::show();

	/*
	 * Now we want to redirect input events to InputManager every time we enter
	 * this screen
	 */
	engine->getInput()->setInputListener(inputManager);
}

void MainScreen::hide() {
	jorge::Log::d("MainScreen hide()");
	Screen::hide();
}

void MainScreen::stop() {
	jorge::Log::d("MainScreen stop()");
	Screen::stop();
}

void MainScreen::goToShapesScreen() {
	game->setScreen(game->shapesScreen, false);
}

MainScreen::~MainScreen() {
	jorge::Log::d("MainScreen delete");
	delete ball;
	delete inputManager;
}
