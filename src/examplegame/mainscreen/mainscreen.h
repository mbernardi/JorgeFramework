#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <jorge.h>

#include "ball.h"
#include "inputmanager.h"
#include "../examplegame.h"

class ExampleGame; // Because of circular includes
class InputManager; // Because of circular includes

class MainScreen : public jorge::Screen {
	public:
		Ball* ball = nullptr;
		jorge::ResourcesManager* res = nullptr;

		MainScreen(jorge::Engine* engine, ExampleGame* game,
				jorge::ResourcesManager* res);

		void start();

		void loop(double& dt);

		void show();

		void hide();

		void stop();

		void goToShapesScreen();

		~MainScreen();

	private:
		jorge::Engine* engine = nullptr;
		ExampleGame* game = nullptr;
		jorge::Texture* tex = nullptr;

		InputManager* inputManager = nullptr;
};

#endif
