#include "ball.h"

#include <string>
#include <jorge/log.h>

Ball::Ball(jorge::Vector2 pos) : pos(pos), speed(500),
		movingLeft(false), movingRight(false) { }

void Ball::update(float dt) {
	pos.x += speed * dt;

	if (movingLeft && !movingRight) {
		speed = -500;
	}
	else if (movingRight && !movingLeft) {
		speed = 500;
	}
	else { // Because is not moving or because is moving both ways
		speed = 0;
	}

	// Do not let it go out of screen
	if (pos.x > 700) {
		pos.x = 700;
	}
	else if (pos.x < 100) {
		pos.x = 100;
	}
}

void Ball::setMovingLeft(bool status) {
	movingLeft = status;
}

void Ball::setMovingRight(bool status) {
	movingRight = status;
}

jorge::Vector2 Ball::getPosition() {
	return pos;
}
