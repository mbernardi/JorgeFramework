#include "shapesscreen.h"

#include <jorge.h>

#include "shapes.h"
#include "../examplegame.h"

ShapesScreen::ShapesScreen(jorge::Engine* engine, ExampleGame* game,
		jorge::ResourcesManager* res)
		: engine(engine), game(game), res(res) { }

void ShapesScreen::start() {
	jorge::Log::d("ShapesScreen start()");
	Screen::start();

	// Set this as the imput listener
	input = engine->getInput();
	input->setInputListener(this);

	// Create shapes
	rect1 = new RectangleShape(100, 100, 100, 150, jorge::Rectangle::ALIGN_CENTER);
	rect2 = new RectangleShape(600, 100, 150, 100, jorge::Rectangle::ALIGN_CENTER);
	circle1 = new CircleShape(100, 400, 50);
	circle2 = new CircleShape(600, 400, 75);

	// Create colors
	red = jorge::Color(255, 0, 0, 255);
	blue = jorge::Color(0, 0, 255, 255);
}

void ShapesScreen::loop(double& dt) {
	// Draw background
	res->getTexture("background")->draw(engine,
			jorge::Rectangle(0, 0, 800, 600));

	// Update shapes
	rect1->update(input->getMousePosition());
	rect2->update(input->getMousePosition());
	circle1->update(input->getMousePosition());
	circle2->update(input->getMousePosition());

	// Check collisions
	bool rect1IsColliding =
			rect1->checkCollision(rect2) ||
			rect1->checkCollision(circle1) ||
			rect1->checkCollision(circle2);

	bool rect2IsColliding =
			rect2->checkCollision(rect1) ||
			rect2->checkCollision(circle1) ||
			rect2->checkCollision(circle2);

	bool circle1IsColliding =
			circle1->checkCollision(rect1) ||
			circle1->checkCollision(rect2) ||
			circle1->checkCollision(circle2);

	bool circle2IsColliding =
			circle2->checkCollision(rect1) ||
			circle2->checkCollision(rect2) ||
			circle2->checkCollision(circle1);

	// Draw shapes
	if (rect1IsColliding) {
		rect1->draw(engine, red);
	}
	else {
		rect1->draw(engine, blue);
	}

	if (rect2IsColliding) {
		rect2->draw(engine, red);
	}
	else {
		rect2->draw(engine, blue);
	}

	if (circle1IsColliding) {
		circle1->draw(engine, res->getTexture("circleColliding"));
	}
	else {
		circle1->draw(engine, res->getTexture("circleNormal"));
	}

	if (circle2IsColliding) {
		circle2->draw(engine, res->getTexture("circleColliding"));
	}
	else {
		circle2->draw(engine, res->getTexture("circleNormal"));
	}
}

void ShapesScreen::show() {
	jorge::Log::d("ShapesScreen show()");
	Screen::show();
}

void ShapesScreen::hide() {
	jorge::Log::d("ShapesScreen hide()");
	Screen::hide();
}

void ShapesScreen::stop() {
	jorge::Log::d("ShapesScreen stop()");
	Screen::stop();
}

void ShapesScreen::keyDown(jorge::Input::Keycode key) {
	switch (key) {
		// Move ball to the right
		case jorge::Input::KEY_ESCAPE:
			game->setScreen(game->mainScreen, false);
			break;

		default:
			break;
	}
}

void ShapesScreen::keyUp(jorge::Input::Keycode key) { }

void ShapesScreen::mouseDown(jorge::Input::Keycode key, jorge::Vector2 pos) {
	// Grab shapes that are below the mouse
	if (rect1->jorgeRect.contains(pos)) {
		rect1->grab();
	}
	if (rect2->jorgeRect.contains(pos)) {
		rect2->grab();
	}
	if (circle1->jorgeCircle.contains(pos)) {
		circle1->grab();
	}
	if (circle2->jorgeCircle.contains(pos)) {
		circle2->grab();
	}
}

void ShapesScreen::mouseUp(jorge::Input::Keycode key, jorge::Vector2 pos) {
	// Drop all shapes
	rect1->drop();
	rect2->drop();
	circle1->drop();
	circle2->drop();
}

ShapesScreen::~ShapesScreen() {
	delete rect1;
	delete rect2;
	delete circle1;
	delete circle2;
}

