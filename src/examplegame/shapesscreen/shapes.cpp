#include "shapes.h"

#include <jorge.h>

void Shape::grab() {
	isGrabbed = true;
}

void Shape::drop() {
	isGrabbed = false;
}

Shape::~Shape() { }

RectangleShape::RectangleShape(float x, float y, float w, float h,
		jorge::Rectangle::alignOptions align) {

	jorgeRect = jorge::Rectangle(x, y, w, h, align);
	isGrabbed = false;
}

void RectangleShape::draw(jorge::Engine* engine, jorge::Color color) {
	jorge::Graphics::setDrawColor(engine, color);
	jorge::Graphics::drawRectangle(engine, jorgeRect, true);
	jorge::Graphics::setDrawColor(engine, jorge::Color(0, 0, 0, 255));
	jorge::Graphics::drawRectangle(engine, jorgeRect, false);
}

void RectangleShape::update(jorge::Vector2 mousePos) {
	if (isGrabbed) {
		jorgeRect.set(mousePos);
	}
}

bool RectangleShape::checkCollision(RectangleShape* rect) {
	return jorge::Math::intersects(jorgeRect, rect->jorgeRect);
}

bool RectangleShape::checkCollision(CircleShape* circle) {
	return jorge::Math::intersects(jorgeRect, circle->jorgeCircle);
}

CircleShape::CircleShape(float x, float y, float r) {
	jorgeCircle = jorge::Circle(x, y, r);
	isGrabbed = false;
}

void CircleShape::draw(jorge::Engine* engine, jorge::Texture* texture) {
	texture->draw(engine, jorge::Rectangle(
			jorgeCircle.x, jorgeCircle.y,
			jorgeCircle.r * 2, jorgeCircle.r * 2,
			jorge::Rectangle::ALIGN_CENTER));
}

void CircleShape::update(jorge::Vector2 mousePos) {
	if (isGrabbed) {
		jorgeCircle.set(mousePos);
	}
}

bool CircleShape::checkCollision(RectangleShape* rect) {
	return jorge::Math::intersects(jorgeCircle, rect->jorgeRect);
}

bool CircleShape::checkCollision(CircleShape* circle) {
	return jorge::Math::intersects(jorgeCircle, circle->jorgeCircle);
}



