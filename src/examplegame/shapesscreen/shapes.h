#ifndef SHAPES_H
#define SHAPES_H

#include <jorge.h>

class RectangleShape;
class CircleShape;

class Shape {
	public:
		virtual void update(jorge::Vector2 mousePos) = 0;

		virtual bool checkCollision(RectangleShape* rect) = 0;

		virtual bool checkCollision(CircleShape* circle) = 0;

		void grab();

		void drop();

		virtual ~Shape();

	protected:
		bool isGrabbed;
};

class RectangleShape : public Shape {
	public:
		RectangleShape(float x, float y, float w, float h,
				jorge::Rectangle::alignOptions align);

		void draw(jorge::Engine* engine, jorge::Color color);

		void update(jorge::Vector2 mousePos);

		bool checkCollision(RectangleShape* rect);

		bool checkCollision(CircleShape* circle);

		jorge::Rectangle jorgeRect;
};

class CircleShape : public Shape {
	public:
		CircleShape(float x, float y, float r);

		void draw(jorge::Engine* engine, jorge::Texture* texture);

		void update(jorge::Vector2 mousePos);

		bool checkCollision(RectangleShape* rect);

		bool checkCollision(CircleShape* circle);

		jorge::Circle jorgeCircle;
};

#endif
