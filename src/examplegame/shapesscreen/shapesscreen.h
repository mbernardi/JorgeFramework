#ifndef SHAPESSCREEN_H
#define SHAPESSCREEN_H

#include <jorge.h>

#include "shapes.h"
#include "../examplegame.h"

class ExampleGame; // Because of circular includes

class ShapesScreen : public jorge::Screen, public jorge::InputListener {
	public:
		ShapesScreen(jorge::Engine* engine, ExampleGame* game,
				jorge::ResourcesManager* res);

		// Inherited from jorge::Screen
		void start();

		void loop(double& dt);

		void show();

		void hide();

		void stop();

		// Inherited from jorge::InputListener
		void keyDown(jorge::Input::Keycode key);

		void keyUp(jorge::Input::Keycode key);

		void mouseDown(jorge::Input::Keycode key, jorge::Vector2 pos);

		void mouseUp(jorge::Input::Keycode key, jorge::Vector2 pos);

		~ShapesScreen();

	private:
		jorge::Engine* engine = nullptr;
		ExampleGame* game = nullptr;
		jorge::ResourcesManager* res = nullptr;
		jorge::Input* input = nullptr;

		RectangleShape* rect1 = nullptr;
		RectangleShape* rect2 = nullptr;
		CircleShape* circle1 = nullptr;
		CircleShape* circle2 = nullptr;

		jorge::Color red;
		jorge::Color blue;
};

#endif
