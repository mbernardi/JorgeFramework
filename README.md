JorgeFramework
==============
Simple C++ game framework based on SDL

Introduction
--------------------------------------

The framework is basically finished, but I've not tested it that much

This framework is designed to make C++ games easer. It's built on top of **SDL**, **SDL_image**, **SDL_ttf** and **SDL_mixer**, hiding SDL behind classes and functions. No knowledge of SDL is needed.  
If you are starting with C++, SDL or game programming maybe this is the best way to start.

Not every part of SDL is implemented (see features section), this framework is meant to build simple 2D games, but it's easy to add more functionality. Feel free to modify it to your needs.

The code comes with a simple example game as tutorial.

Disclaimer
----------

I'm not a native English speaker, also this is my first time programming in C++ and SDL. Though I think this framework it's not that bad :P

Also
----

Some parts were made following Twinklebear SDL tutorial, check it out:

[Tutorial webpage](http://www.willusher.io/pages/sdl2/)  
[Tutorial Github](https://github.com/Twinklebear/TwinklebearDev-Lessons)

A bit inspired in LibGDX and the "LibGDX" book [Beggining Android Games](http://www.apress.com/9781430246770) (also collision checking for circles and rectangles is from there).

The **FindSDL2.cmake**, **FindSDL2_image.cmake** and **FindSDL2_ttf.cmake** are from Twinklebear's tutorial.  
The **FindSDL2_mixer.cmake** was modified based on above modules temporairly until some official module is published.

Features
--------

- Implements the main loop, your code goes inside your game or screen loop() method
- 2D Texture rendering
- Text rendering
- Rendering rectangles, lines and points
- Sound
- Music (Only one at once, it's an SDL_mixer limitation)
- File reading and writing
- Game and Screen base classes, implementing screen switching and related methods (calls screen.hide(), screen.show(), etc.)
- Keyboard events
- Mouse events and polling
- Printing to console
- A resources manager (a simple class containing the resources you need)
- 2D vector, rectangle and circle classes (including collision checking)

Dependencies
------------

### For running

You need **SDL**, **SDL_image**, **SDL_ttf** and **SDL_mixer**. In Debian you can install them with:
```
sudo aptitude install libsdl2-2.0-0 libsdl2-image-2.0-0 libsdl2-mixer-2.0-0 libsdl2-ttf-2.0-0
```

### For compiling

You need **make**, **cmake** and the dev packages for **SDL**, **SDL_image**, **SDL_ttf** and **SDL_mixer**. In Debian you can:
```
sudo aptitude install build-essential cmake libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-mixer-dev
```

How to build/run
----------------

Tested only in GNU/Linux, but I use cmake and SDL so it should work on Windows too

Create two folders: `bin` and `build` in root folder
```
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../src/
make

cd ../bin
./main
```
Run with `./main --debug` if you want verbose output

If you want to share your game you can build it and share the `bin` and `res` folders

How a game is structured
------------------------

The engine is part of the framework. Your code goes inside the game, the screens
or in your classes

Basically, the engine calls the following methods in your game:

- start()
- loop()
- stop()

Then, the game calls the following methods in the screens:

- start()
- loop()
- show()
- hide()
- stop()

### A simple game works like this:

- A main.cpp who:
	- Creates a game
	- Creates an engine and passes a pointer of that game to the engine

- The engine initializes SDL and calls game's start()
- The game.start(), creates all the screens and sets one as the current screen.
  When the screen is set, screen.start() and screen.show() are called by the
  game
- The screen.start() loads objects and sets things up
- When game.start() finishes, the engine enters the main loop and starts
  calling game.loop()
- The game.loop() calls screen.loop() and there everything happens

- When changing screens, the screen calls game.setScreen(screen)
- If stop is given, the old screen's hide and stop method are called. Also,
  start() and show() are called in the new screen
- If stop is false, only hide() is called in the old screen. Also when setting
  a screen that was hidden but not stopped, only show() is called at start
- From now on game.loop() calls the new screen loop()

- If a screen wants to quit the game calls game.quit(), stopping the
  engine

- When the engine stops calls game.stop(), and game.stop() calls
  screen.stop(). stop() is not called on hidden screens

- All screens are deleted when the game is deleted by main.cpp
